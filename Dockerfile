# NASA FDL 2018 Astrobiology Team 2
# INARA: Intelligent exoplaNet Atmosphere RetrievAl
#
# July 2018

FROM nvidia/cuda:9.0-cudnn7-devel-ubuntu16.04

ENV PYTHON_VERSION=3.6

RUN apt-get update && apt-get install -y --no-install-recommends \
        git \
        curl \
        ca-certificates &&\
    rm -rf /var/lib/apt/lists/*

RUN curl -o ~/miniconda.sh -O  https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh  && \
     chmod +x ~/miniconda.sh && \
     ~/miniconda.sh -b -p /opt/conda && \
     rm ~/miniconda.sh
ENV PATH /opt/conda/bin:$PATH

RUN pip install --upgrade numpy scipy pandas astropy matplotlib google-cloud-storage
RUN pip install http://download.pytorch.org/whl/cu90/torch-0.4.0-cp36-cp36m-linux_x86_64.whl
RUN pip install torchvision
RUN pip install git+https://github.com/gbaydin/pypsg.git

COPY run.py /home
COPY genparams /home/genparams
COPY makenumpy.py /home
COPY convert_csv_npy.py /home

WORKDIR /home
CMD tail -f /dev/null
#RUN bash -c 'echo -e RUNNING ENSEMBLE NOW'
#CMD python ensemble_nn.py
#CMD python run.py --root $ROOT --mode $MODE
