#!/usr/bin/env bash
if docker build -t inara .  ; then
  docker tag inara gcr.io/astrobiology-team-2/inara
  docker push gcr.io/astrobiology-team-2/inara

  docker tag inara registry.gitlab.com/frontierdevelopmentlab/astrobiology/inara
  docker push registry.gitlab.com/frontierdevelopmentlab/astrobiology/inara
fi
