# INARA: Intelligent exoplaNet Atmosphere RetrievAl

NASA FDL 2018 Astrobiology Team 2

This tool is a complete solution for the generation of rocky planets spectra and the training of a machine learning model for atmosphere parameters retrial. 
Spectra generation produces high resolution spectra and then observation simulation is also saved for the requested number of randomly generated planets across parameters space (see appendix A below). This mode can be used to save data for learning, validation and test, using different options (see usage sections below)

Generated spectra (observation simulations) can be used to train a machine learning model in train mode (see usage section). Models can be trained as a single model or in ensemble mode. 

A test mode is also available once a model has been trained and test data has been generated.

Two other modes are available to instantiate virtual machines (VMs) to either generate planetary spectra or the PSG (see NASA PSG, Planetary Spectra Generator) on the Google Cloud. The VMs are instantiated on hard-coded parameters loading docker images and running the command line parameters. See below.

The intended use however is the local one (not the google cloud) where the user downloads the generated data from the cloud and runs or develop new ML models, test them and once real telescope data will be available will use the models for efficient and quick atmospheric retrieval. 


## Installation

1. Ensure you have a working version of 3.+. We recommend using Python from the [Anaconda Python distribution](https://www.continuum.io/downloads) for a quicker and more reliable experience. However, if you have Python already installed that will probably work fine too.

2. Gii clone this repository

3. ***Use yaml file to get all python required libaries***



## Usage

The following examples are for instructing general usage. Run `python run.py --help` for a full list of options and explanations.

*Note about using Google Cloud Storage (GCS) or the local file system:*

The `--root` argument determines the root location of file operations. In the following examples we use `--root gs://inara-debug-7b`, meaning that the code will work with a GCS bucket called `inara-debug-7b` (the bucket needs to exist, i.e., it needs to be manually created).

If one uses a local file system path such as `--root .` or `--root /home/inara_test/` all operations will work under that path in the local file system with no GCS access.

## 1. Generate data for training and validation

First we generate data for training.

```
python run.py --root gs://inara-debug-7b --mode datagen --datagen_files 2 --datagen_planets_per_file 10 --psg_url https://psg.gsfc.nasa.gov/api.php --datagen_dir train
```

Next we generate some data for validation. We do the same with above, the only difference being that we write the data to the `valid` directory.

```
python run.py --root gs://inara-debug-7b --mode datagen --datagen_files 2 --datagen_planets_per_file 10 --psg_url https://psg.gsfc.nasa.gov/api.php --datagen_dir valid
```

Note: `--datagen_files 2 --datagen_planets_per_file 10` means we will generate two files with ten planets in each file. In a real data generation run, `--datagen_files` for the training set should be something like 10,000s to generate a training set of 100,000s of planets. For the validation set, a size of `--datagen_files 100` should be enough.


## 2. Train a machine learning model

### Option A: Train a single model

The following will train a feed-forward neural network for 4 epochs with a minibatch size of 4, validate every 2 iterations, and save resulting model snapshots into the `model` directory.

```
python run.py --root gs://inara-debug-7b --mode train --train_model_type FF --minibatch_size 4 --train_epochs 3 --valid_iter 2 --model_dir model
```

The model options are (with potentially others to be added later):
* `LR`: linear regression
* `FF`: feed-forward neural network with 3 layers
* `CNN`: a 1d convolutional neural network with 6 convolutional layers

See code for the details of these models.

### Option B: Train an ensemble of models

If you run multiple training instances with different random number seeds, and save snapshots of them to the same `--model_dir`, they can later be executed as an ensemble.

```
python run.py --root gs://inara-debug-7b --mode train --train_model_type FF --minibatch_size 8 --train_epochs 3 --valid_iter 2 --seed 0
python run.py --root gs://inara-debug-7b --mode train --train_model_type FF --minibatch_size 8 --train_epochs 3 --valid_iter 2 --seed 1
python run.py --root gs://inara-debug-7b --mode train --train_model_type FF --minibatch_size 8 --train_epochs 3 --valid_iter 2 --seed 2
...
```

## 3. Generate data for testing a trained model

We generate a number of test input files and save them to the `test` directory.

```
python run.py --root gs://inara-debug-7b --mode datagen --datagen_files 1 --datagen_planets_per_file 2 --psg_url https://psg.gsfc.nasa.gov/api.php --datagen_dir test
```

## 4. Test a trained machine learning model (or an ensemble of several trained models)

We run the script with `--mode test`, which does the following:
* Checks all test input files in `--test_dir`
* Checks the saved model files in the `--model_dir`
* Determines the number of distinct model training runs, based on the random number seeds in each trained model
* Iterates through the distinct random number seeds and executes the latest model snapshot for each random number seed with all test input files
* Saves the corresponding test output files with suffixes such as `_output_seed_0` to a `/predictions` directory under the `--test_dir` directory


```
python run.py --root gs://inara-debug-7b --mode test