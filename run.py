# -*- coding: utf-8 -*-
# NASA FDL 2018 Astrobiology Team 2
# INARA: Intelligent exoplaNet Atmosphere RetrievAl
#
# July 2018
import traceback
import argparse
import numpy as np
import os
import uuid
import csv
import sys
import io
import glob
import pprint
import datetime
import time
import random
import tempfile
from google.cloud import storage
from io import StringIO
import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from genparams import params
import pypsg
import requests
import re
import matplotlib.pyplot as plt
plt.switch_backend('agg')

# Observation type
obs_type  = 'do'  #'do' for direct observations or 'tr' for transit
cloud_sim = False #boolean flag to determine whether or not to simulate clouds
# Observation dimension (neural network input)
observation_dim = 4379 # observation size, no clouds, final decision data size
full_dim = 15346    # total spectrum size, no clouds, final decision data size
# 2696
# 11515
# Parameter dimension (the full parameter range)
params_dim = 28 # 27  # 39
# The subset of parameters that the neural network predicts
# Upper limit not included (PyTorch slicing)
params_slice = [14, 26]  # [13, 25] # [25, 36]
epsilon = 10e-33

# Calling PSG & generating parameters and obersversion pairs
def get_planet(psg, name=''):
    psg_result = params.callpsg(psg, name, obs_type, cloud_sim)
    par = psg_result[0]
    obs = psg_result[1]
    config = psg_result[-1]  # 4
    return par, obs, config


# Timestamp
def get_time_stamp():
    # return datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d_%H%M%S')
    # Use number of elapsed seconds since Epoch, i.e., an increasing integer that is the same in any time zone. Helps with working on experiments from machines with different local times.
    return str(int(time.time()))


# Converts a given duration (seconds) to a string of days, hours, minutes, seconds
def days_hours_mins_secs_str(total_seconds):
    d, r = divmod(total_seconds, 86400)
    h, r = divmod(r, 3600)
    m, s = divmod(r, 60)
    return '{0} days {1:02}:{2:02}:{3:02}'.format(int(d), int(h), int(m), int(s))


# Returns a tensor where all rows in which a nan is present are removed
def remove_rows_with_nan(tensor):
    nans = torch.isnan(tensor).nonzero()
    if nans.nelement() > 0:
        rows_with_nan = list(set(list(nans[:, 0].numpy())))
        print('Removing {} row(s) with nan'.format(len(rows_with_nan)))
        buffer = torch.Tensor()
        for i in range(tensor.size(0)):
            if i not in rows_with_nan:
                buffer = torch.cat([buffer, tensor[i]])
        return buffer.view(-1, tensor.size(1))
    return tensor


# Select the latest model file in a given model_dir (based on the file names that include time stamps)
def get_latest_model(device, model_dir, gcs_bucket=None, model_filename_prefix=None):
    if model_filename_prefix is None:
        model_filename_prefix = 'inara_'

    if gcs_bucket is not None:
        # Get the latest model from bucket
        model_list = list(gcs_bucket.list_blobs(prefix=os.path.basename(model_dir)))
        # print(model_list)
        model_list = [model for model in model_list if model_filename_prefix in model.name]
        sorted_model_list = sorted(model_list, key=lambda model: os.path.basename(model.name))
        # print(sorted_model_list)
        model_blob = sorted_model_list[-1]
        tmp_file = tempfile.NamedTemporaryFile()
        model_blob.download_to_file(tmp_file)
        # Setting the data pointer in the file
        tmp_file.seek(0)
        # Load the saved model
        model_dict = torch.load(tmp_file)
        model_filename = model_blob.name
    else:
        # Same as above just from file system
        model_list = glob.glob(os.path.join(model_dir, model_filename_prefix + '*'))
        if len(model_list) == 0:
            print('No model files found.')
            sys.exit(1)
        else:
            sorted_model_list = sorted(model_list)
            model_filename = sorted_model_list[-1]
            model_dict = torch.load(model_filename)
    print('Loaded model:', model_filename)
    return model_filename, model_dict


# Get a string with information about a previously trained model
def get_model_info(model_dict):
    ret = ['Model type             : {}'.format(model_dict['model_type']),
           'Training epochs        : {:,}'.format(model_dict['epoch']),
           'Training iterations    : {:,}'.format(model_dict['iteration']),
           'Training minibatch size: {:,}'.format(model_dict['minibatch_size']),
           'Seed                   : {}'.format(model_dict['seed']),
           'Initial training loss  : {:.6E}'.format(model_dict['train_loss_history'][0]),
           'Final training loss    : {:.6E}'.format(model_dict['train_loss_history'][-1]),
           'Minimum training loss  : {:.6E}'.format(model_dict['train_loss_min']),
           'Initial validation loss: {:.6E}'.format(model_dict['valid_loss_history'][0]),
           'Final validation loss  : {:.6E}'.format(model_dict['valid_loss_history'][-1]),
           'Minimum validation loss: {:.6E}'.format(model_dict['valid_loss_min'])]
    return '\n'.join(ret)


# Machine learning models
class LinearRegression(nn.Module):
    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.fc1 = nn.Linear(input_dim, output_dim)

    def forward(self, x):
        x = self.fc1.forward(x)
        # Logistic regression:
        # x = F.sigmoid(x)
        return x


class FeedForwardNet1(nn.Module):
    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.fc1 = nn.Linear(input_dim, 256)
        self.fc2 = nn.Linear(256, 128)
        self.fc3 = nn.Linear(128, output_dim)
        self.config_str = ''

    def forward(self, x):
        #import pdb; pdb.set_trace()
        x = self.fc1.forward(x)
        x = F.relu(x)
        x = self.fc2.forward(x)
        x = F.relu(x)
        x = F.dropout(x, 0.2, training = self.training)
        x = self.fc3.forward(x)
        #x = F.dropout(x, 0.2, training = self.training)
        x = F.softmax(x, dim=1)  # this is only for the time-being when we are predicting concentrations, which are all in [0,1] and sum up to 1.
        return x

class FeedForwardNet2(nn.Module):
    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.fc1 = nn.Linear(input_dim, 256)
        self.fc2 = nn.Linear(256, 128)
        self.fc3 = nn.Linear(128, 128)
        self.fc4 = nn.Linear(128, output_dim)
        self.config_str = ''

    def forward(self, x):
        #import pdb; pdb.set_trace()
        x = self.fc1.forward(x)
        x = F.leaky_relu(x)
        x = self.fc2.forward(x)
        x = F.leaky_relu(x)
        x = self.fc3.forward(x)
        x = F.relu(x)
        x = F.dropout(x, 0.2, training = self.training)
        x = self.fc4.forward(x)
        #x = F.dropout(x, 0.2, training = self.training)
        x = F.softmax(x, dim=1)  # this is only for the time-being when we are predicting concentrations, which are all in [0,1] and sum up to 1.
        return x


class ConvNet1(nn.Module):
    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.conv1 = nn.Conv1d(1, 64, kernel_size=3)
        self.conv2 = nn.Conv1d(64, 64, kernel_size=3)
        self.conv3 = nn.Conv1d(64, 128, kernel_size=3)
        self.conv4 = nn.Conv1d(128, 128, kernel_size=3)
        self.conv5 = nn.Conv1d(128, 128, kernel_size=3)
        test_input = torch.Tensor(1, 1, input_dim)
        test_cnn_output = self._forward_cnn(test_input)
        self._cnn_output_dim = test_cnn_output.view(-1).size(0)
        self.linear1 = nn.Linear(self._cnn_output_dim, 1024)
        self.linear2 = nn.Linear(1024, 1024)
        self.linear3 = nn.Linear(1024, output_dim)

    def _forward_cnn(self, x):
        # import pdb; pdb.set_trace()
        x = self.conv1.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=3)(x)
        x = self.conv2.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=3)(x)
        x = self.conv3.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=3)(x)
        x = self.conv4.forward(x)
        x = torch.relu(x)
        x = self.conv5.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=3)(x)
        return x

    def forward(self, x):
        x = x.unsqueeze(1)
        x = self._forward_cnn(x)
        x = x.view(-1, self._cnn_output_dim)
        x = self.linear1.forward(x)
        x = torch.relu(x)
        x = self.linear2.forward(x)
        x = torch.relu(x)
        x = self.linear3.forward(x)
        x = F.softmax(x, dim=1)  # this is only for the time-being when we are predicting concentrations, which are all in [0,1] and sum up to 1.
        return x


class ConvNet2(nn.Module):
    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.conv1 = nn.Conv1d(1, 64, kernel_size=3)
        self.conv2 = nn.Conv1d(64, 64, kernel_size=3)
        self.conv3 = nn.Conv1d(64, 128, kernel_size=3)
        self.conv4 = nn.Conv1d(128, 128, kernel_size=3)
        self.conv5 = nn.Conv1d(128, 128, kernel_size=3)
        self.conv6 = nn.Conv1d(128, 128, kernel_size=3)
        test_input = torch.Tensor(1, 1, input_dim)
        test_cnn_output = self._forward_cnn(test_input)
        self._cnn_output_dim = test_cnn_output.view(-1).size(0)
        self.linear1 = nn.Linear(self._cnn_output_dim, 1024)
        self.linear2 = nn.Linear(1024, output_dim)

    def _forward_cnn(self, x):
        # import pdb; pdb.set_trace()
        x = self.conv1.forward(x)
        x = self.conv2.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=3)(x)
        x = self.conv3.forward(x)
        x = self.conv4.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=3)(x)
        x = self.conv5.forward(x)
        x = self.conv6.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=3)(x)
        return x

    def forward(self, x):
        x = x.unsqueeze(1)
        x = self._forward_cnn(x)
        x = x.view(-1, self._cnn_output_dim)
        x = self.linear1.forward(x)
        x = torch.relu(x)
        x = self.linear2.forward(x)
        x = F.softmax(x)  # this is only for the time-being when we are predicting concentrations, which are all in [0,1] and sum up to 1.
        return x

class ConvNet3(nn.Module):
    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.conv1 = nn.Conv1d(1, 64, kernel_size=2)
        self.conv2 = nn.Conv1d(64, 64, kernel_size=2)
        self.conv3 = nn.Conv1d(64, 128, kernel_size=2)
        self.conv4 = nn.Conv1d(128, 128, kernel_size=2)
        self.conv5 = nn.Conv1d(128, 128, kernel_size=2)
        self.conv6 = nn.Conv1d(128, 128, kernel_size=2)
        self.conv7 = nn.Conv1d(128, 128, kernel_size=2)
        self.conv8 = nn.Conv1d(128, 128, kernel_size=2)
        test_input = torch.Tensor(1, 1, input_dim)
        test_cnn_output = self._forward_cnn(test_input)
        self._cnn_output_dim = test_cnn_output.view(-1).size(0)
        self.linear1 = nn.Linear(self._cnn_output_dim, 1024)
        self.linear2 = nn.Linear(1024, output_dim)

    def _forward_cnn(self, x):
        # import pdb; pdb.set_trace()
        x = self.conv1.forward(x)
        x = self.conv2.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=2)(x)
        x = self.conv3.forward(x)
        x = self.conv4.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=2)(x)
        x = self.conv5.forward(x)
        x = self.conv6.forward(x)
        x = torch.relu(x)
        x = self.conv7.forward(x)
        x = self.conv8.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=2)(x)
        return x

    def forward(self, x):
        x = x.unsqueeze(1)
        x = self._forward_cnn(x)
        x = x.view(-1, self._cnn_output_dim)
        x = self.linear1.forward(x)
        x = torch.relu(x)
        x = self.linear2.forward(x)
        x = F.softmax(x)  # this is only for the time-being when we are predicting concentrations, which are all in [0,1] and sum up to 1.
        return x

class ConvNet4(nn.Module):
    def __init__(self, input_dim, output_dim):
        super().__init__()
        self.conv1 = nn.Conv1d(1, 64, kernel_size=3)
        self.conv2 = nn.Conv1d(64, 64, kernel_size=3)
        self.conv3 = nn.Conv1d(64, 128, kernel_size=3)
        self.conv4 = nn.Conv1d(128, 256, kernel_size=3)
        test_input = torch.Tensor(1, 1, input_dim)
        test_cnn_output = self._forward_cnn(test_input)
        self._cnn_output_dim = test_cnn_output.view(-1).size(0)
        self.linear1 = nn.Linear(self._cnn_output_dim, 1024)
        self.linear2 = nn.Linear(1024, output_dim)

    def _forward_cnn(self, x):
        # import pdb; pdb.set_trace()
        x = self.conv1.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=3)(x)
        x = self.conv2.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=3)(x)
        x = self.conv3.forward(x)
        x = torch.relu(x)
        x = self.conv4.forward(x)
        x = torch.relu(x)
        x = nn.MaxPool1d(kernel_size=3)(x)
        return x

    def forward(self, x):
        x = x.unsqueeze(1)
        x = self._forward_cnn(x)
        x = x.view(-1, self._cnn_output_dim)
        x = self.linear1.forward(x)
        x = torch.relu(x)
        x = F.dropout(x, 0.2, training = self.training)
        x = self.linear2.forward(x)
        # No linear activation function here! Absence of an activation function is 'a linear implementation' this is only for the time-being when we are predicting concentrations, which are all in [0,1] and sum up to 1.
        return x

# dataloader class - see inline comments
class DataLoader():
    # Initialising
    def __init__(self, data_dir=None, gcs_bucket=None, max_blobs=None):

        # If bucket is not given then work with file system
        if gcs_bucket is None:
            self._gcs_enabled = False
            self._data_dir = data_dir
            # Get a list of all the files in the training folder
            print('Listing files in dir   : {} ...'.format(data_dir))
            self._files = glob.glob(os.path.join(self._data_dir, '*.npy'))
            if len(self._files) == 0:
                self._files = glob.glob(os.path.join(self._data_dir, '*.csv'))
            if len(self._files) > 0:
                planets_per_file = self.read_data_from_file(self._files[0]).size(0)
        # If bucket is given work with google cloud bucket
        else:
            self._gcs_enabled = True
            self._data_dir = os.path.basename(data_dir)
            # Get a list of all the blobs in the training folder
            # The following call to list_blobs is significantly slow for a bucket containing a large number of files. A faster alternative can be implemented.
            print('Listing blobs in dir   : {} ...'.format(data_dir))
            print('This might take up to several minutes if there are many blobs. A faster implementation is pending.')
            self._files = list(gcs_bucket.list_blobs(prefix=self._data_dir, max_results=max_blobs))
            self._files = [file for file in self._files if 'csv' in file.name]
            if len(self._files) > 0:
                planets_per_file = self.read_data_from_blob(self._files[0]).size(0)

        print('Total number of files  : {:,}'.format(len(self._files)))
        if len(self._files) > 0:
            print('Total number of planets: {:,} (assuming {:,} planets per file)'.format(len(self._files) * planets_per_file, planets_per_file))

        # Creating a buffer by creating a Tensor (empty tensor)
        self._buffer = torch.Tensor()
        self.epoch = 1
        self._file_index = 0
        self.num_files = len(self._files)
        random.shuffle(self._files)

    # General writing and reading methods reused in later parts of the program
    @staticmethod
    def read_data_from_blob(blob):
        data_string = blob.download_as_string().decode('utf-8')
        # Creating a torch tensor here from a single blob (csv format)
        # return torch.from_numpy(pd.read_csv(, delimiter='\t', header=None).as_matrix()).float()
        return torch.from_numpy(np.loadtxt(StringIO(data_string), delimiter='\t')).float()

    @staticmethod
    def read_data_from_file(file_name):
        # Creating a torch tensor here from a single file (csv format)
        # return torch.from_numpy(pd.read_csv(file_name, delimiter='\t', header=None, engine='c', na_filter=False).as_matrix()).float()
        # return torch.from_numpy(pd.read_csv(file_name, delimiter='\t', header=None, engine='c', na_filter=False, low_memory=False, quoting = csv.QUOTE_NONE, index_col = False).as_matrix()).float()
        if '.csv' in file_name:
            npy_file_name = file_name.replace('.csv', '.npy')
            if os.path.exists(npy_file_name):
                data = torch.from_numpy(np.load(npy_file_name)).float()
            else:
                data = torch.from_numpy(np.loadtxt(file_name, delimiter='\t')).float()
        else:
            data = torch.from_numpy(np.load(file_name)).float()
        return data

    @staticmethod
    def write_data_to_file(data, file_name):
        if 'csv' in file_name:
            stream = io.StringIO()
            writer = csv.writer(stream, delimiter='\t', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
            for i in range(data.size(0)):
                row = data[i].detach().cpu().numpy()
                writer.writerow(row)
            with open(file_name, 'w') as file:
                file.write(stream.getvalue())
        else:
            np.save(file_name, data.detach().cpu().numpy())

    @staticmethod
    def write_data_to_blob(data, blob):
        stream = io.StringIO()
        writer = csv.writer(stream, delimiter='\t', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
        for i in range(data.size(0)):
            row = data[i].detach().cpu().numpy()
            writer.writerow(row)
        blob.upload_from_string(stream.getvalue())

    # Dangerous method! return all of your data as single tensor - good for validation set but careful!
    def get_all(self):
        all_data = []
        for file in self._files:
            if self._gcs_enabled:
                data = self.read_data_from_blob(file)
            else:
                data = self.read_data_from_file(file)
            data = remove_rows_with_nan(data)
            all_data.append(data)
        return torch.cat(all_data)

    # Creating minibatches from the buffer, if buffer does not have ebough rows it will fill the buffer
    def get_minibatch(self, size=64):
        # Check if we have enough rows in the buffer to create a minibatch
        if self._buffer.size(0) >= size:
            # print('buffer has enough')
            # There are enough rows in the buffer
            # Slice the required number of rows as the minibatch to return
            ret = self._buffer[:size]
            # Remove the sliced minibatch rows from the buffer
            self._buffer = self._buffer[size:]
            return ret
        else:
            # There are not enough rows in the buffer
            # Start a loop for reading files from the files sequence and adding the rows to the buffer
            buffer_ready = False
            while not buffer_ready:
                # print('buffer hasn\'t enough. {} / {}'.format(self._buffer.size(0), size))
                # print('reading new file...')
                if self._gcs_enabled:
                    data = self.read_data_from_blob(self._files[self._file_index])
                else:
                    data = self.read_data_from_file(self._files[self._file_index])
                # print('new file read.')
                # print('removing nans...')
                data = remove_rows_with_nan(data)
                # print('nans removed.')
                # print('concatting to buffer...')
                self._buffer = torch.cat([self._buffer, data])
                # print('concat to buffer done.')

                # Check if the buffer has enough rows now to supply the minibatch
                # If yes, this will terminate the buffer-filling loop
                if self._buffer.size(0) >= size:
                    # print('buffer now has enough. {}'.format(self._buffer.size(0)))
                    buffer_ready = True

                # Check if we reached the end of the list of files
                # If yes, rewind to the first file, increment the epoch
                self._file_index += 1
                if self._file_index >= len(self._files):
                    # print('finished epoch, shuffling...')
                    random.shuffle(self._files)
                    # print('shuffling done.')
                    self._file_index = 0
                    self.epoch += 1
            # Recursive call
            return self.get_minibatch(size)


def main():
    try:
        parser = argparse.ArgumentParser(description='NASA FDL 2018 Astrobiology Team 2 PSG Training', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument('--datagen_dir', help='Directory for saving generated data (will be created if not existing)', default='train', type=str)
        parser.add_argument('--train_dir', help='Directory for training data', default='train', type=str)
        parser.add_argument('--model_dir', help='Directory for trained models (will be created if not existing)', default='model', type=str)
        parser.add_argument('--valid_dir', help='Directory for validation data', default='valid', type=str)
        parser.add_argument('--test_dir', help='Directory for test data', default='test', type=str)
        parser.add_argument('--root', help='The root file system directory (a local file system path) or Google Cloud Storage bucket (a URI starting with gs://)', default='.', type=str)
        parser.add_argument('--datagen_files', help='Number of files to generate', default=4, type=int)
        parser.add_argument('--datagen_planets_per_file', help='Number of planets per file to generate', default=8, type=int)
        parser.add_argument('--train_epochs', help='Number of epochs to train', default=5, type=int)
        parser.add_argument('--train_early_stop_tolerance', help='Number of times the validation loss is allowed not to improve before stopping the training', default=15, type=int)
        parser.add_argument('--seed', help='Random number seed', default=None, type=int)
        parser.add_argument('--minibatch_size', help='Size of training minibatches', default=64, type=int)
        parser.add_argument('--learning_rate', help='Learning rate for training', default=0.0001, type=float)
        parser.add_argument('--weight_decay', help='L2 regularization (weight decay) for training', default=1e-5, type=float)
        parser.add_argument('--mode', help='Main mode (datagen: data generation, stats: print data statistics, train: train the model, test: test the trained model with new data)', choices=['datagen', 'stats', 'train', 'test', 'createvm', 'createpsg'], nargs='?', default='datagen', type=str)
        parser.add_argument('--createvm_name', help='The name of VM instance to be created', default=None, type=str)
        parser.add_argument('--createvm_command', help='The command to execute in the VM created', default=None, type=str)
        parser.add_argument('--createpsg_name', help='Start a PSG server VM', default=None, type=str)
        parser.add_argument('--valid_size', help='Number of planets in the validation set', default=128, type=int)
        parser.add_argument('--valid_iter', help='Interval (iterations) for validation and model saving', default=10, type=int)
        parser.add_argument('--stats_planets', help='The number of planets to sample from the dataset to compute data statistics', default=100, type=int)
        parser.add_argument('--log_obs_mean', help='Global mean for log observation normalization, applies to training and testing', default=-46.61, type=float)
        parser.add_argument('--log_obs_stddev', help='Global standard deviation for log observation normalization, applies to training and testing', default=2.93, type=float)
        parser.add_argument('--psg_api_key', help='API key for PSG', default=None, type=str)
        parser.add_argument('--psg_url', help='URL for PSG', default='http://35.231.69.180/api.php', type=str)
        parser.add_argument('--cuda', help='Enable CUDA if available', action='store_true')
        parser.add_argument('--train_model_type', help='Type of machine learning model to train (LR: linear regression, FF: feed-forward neural net, CNN1: convolutional neural net, CNN2: convolutional neural net)', choices=['LR', 'FF', 'FF2', 'CNN1', 'CNN2', 'CNN3', 'CNN4'], nargs='?', default='LR', type=str)
        parser.add_argument('--max_blobs', help='Limit the maximum number of blobs that will be read from Google Cloud Storage, for debugging purposes.', type=int, default=None)
        parser.add_argument('--optimiser', help='Select the optimisation algorithm', choices=['Adam', 'SGD', 'ADA', 'RMS'], nargs='?', default='ADAM', type=str)


        # Parser assignment
        opt = parser.parse_args()

        print('NASA FDL 2018 Astrobiology Team 2')
        print('INARA: Intelligent exoplaNet Atmosphere RetrievAl\n')

        print('Arguments:')
        # Pretty print all parameters for this run. Will be displayed at the beginning
        pprint.pprint(vars(opt), depth=2, width=50)
        print()

        if opt.cuda:
            if torch.cuda.is_available():
                print('CUDA is available')
                print('Device: CUDA')
                # The following needs to be updated for using multiple GPUs
                device = torch.device('cuda:0')
            else:
                print('CUDA is not available')
                print('Device: CPU')
                device = torch.device('cpu')
        else:
            print('Device: CPU')
            device = torch.device('cpu')
        print()

        # For reproducability
        if opt.seed is None:
            opt.seed = int(time.time())
            print('No seed given, seeding from time')
        print('Seed  : {}\n'.format(opt.seed))
        random.seed(opt.seed)
        np.random.seed(opt.seed)
        torch.manual_seed(opt.seed)

        # Checking if we are using Google Cloud Storage or local file system
        if opt.root.startswith('gs://'):
            gcs_enabled = True
            bucket_name = opt.root.replace('gs://', '')
            print('Working with Google Cloud Storage')
            print('Connecting to Google Cloud...')
            # Initialising Google Cloud Storage
            gcs_storage_client = storage.Client()
            print('Current buckets in the account:')
            # List all the available buckets
            for bucket in list(gcs_storage_client.list_buckets()):
                print(' ' + bucket.name)
            print('Setting root bucket: {} ...'.format(bucket_name))
            gcs_bucket = gcs_storage_client.get_bucket(bucket_name)
        else:
            gcs_enabled = False
            print('Working with the local file system')
            print('Root directory: {}'.format(opt.root))
            gcs_bucket = None
        print()

        # Data generation mode
        if opt.mode == 'datagen':
            print('\n*** DATA GENERATION MODE ***\n')
            if not gcs_enabled:
                full_path = os.path.join(os.path.join(opt.root, opt.datagen_dir), 'psg_config')
                if not os.path.exists(full_path):
                    print('Directory does not exists, creating: {} ...'.format(full_path))
                    os.makedirs(full_path)
            # Connect to PSG
            psg = pypsg.PSG(server_url=opt.psg_url, timeout_seconds=600, api_key=opt.psg_api_key)
            print()
            # Select how many files we want to generate
            planets = 0
            time_start = time.time()
            for i in range(opt.datagen_files):
                # Unique ID for this collection of planets
                planets_id = 'fdl_{}'.format(uuid.uuid4())
                planets_file_name = '{}.csv'.format(planets_id)
                planets_full_path = os.path.join(opt.datagen_dir, planets_file_name)
                print('Generating file {}/{}...'.format(i + 1, opt.datagen_files))
                # List to save the rows of the files
                rows = []
                for j in range(opt.datagen_planets_per_file):
                    # Unique ID for each planet in this collection of planets
                    planet_name = '{}_{}'.format(planets_id, j+1)
                    print('Generating planet {}/{}: {} ...'.format(j + 1, opt.datagen_planets_per_file, planet_name))
                    try:
                        params, observation, config_str = get_planet(psg, planet_name)
                        planets += 1
                        duration = time.time() - time_start
                        planets_per_second = planets / duration
                        print('Planets/second (mean): {:,.6f}'.format(planets_per_second))
                        print('ETA                  : {}'.format(days_hours_mins_secs_str((opt.datagen_files * opt.datagen_planets_per_file - planets) / planets_per_second)))
                        planet_file_name = '{}.config'.format(planet_name)
                        planet_full_path = os.path.join(opt.datagen_dir, 'psg_config/' + planet_file_name)
                        print('Saving planet config:', planet_full_path)
                        if gcs_enabled:
                            blob = gcs_bucket.blob(planet_full_path)
                            blob.upload_from_string(config_str)
                        else:
                            with open(os.path.join(opt.root, planet_full_path), 'w') as file:
                                file.write(config_str)
                        # Add planet as a row in the list
                        rows.append(torch.from_numpy(np.concatenate([params, observation.flatten('F')])))
                    except requests.exceptions.ReadTimeout:
                        print('PSG timed out, skipping this planet...')
                        continue
                    except Exception:
                        traceback.print_exc(file=sys.stdout)
                        print('PSG error, skipping this planet...')
                        continue
                # Takes list of rows and puts it into a matix
                data = remove_rows_with_nan(torch.stack(rows))
                if data.size(0) == 0:
                    print('No planets generated for this file, skipping...')
                    continue
                print('Saving file: {} ...'.format(planets_full_path))
                print()
                # if we use Google Cloud Storage, write to a bucket, if not write to a file
                if gcs_enabled:
                    blob = gcs_bucket.blob(planets_full_path)
                    DataLoader.write_data_to_blob(data, blob)
                else:
                    DataLoader.write_data_to_file(data, planets_full_path)

        # Model training
        elif opt.mode == 'stats':
            print('\n*** STATISTICS MODE ***\n')
            print('Sample size (planets)     : {:,}'.format(opt.stats_planets))
            print('Sample size (observations): {:,}'.format(opt.stats_planets * observation_dim))
            print()
            # Create the training data loader
            print('Training data')
            train_data_loader = DataLoader(os.path.join(opt.root, opt.train_dir), gcs_bucket, opt.max_blobs)
            if train_data_loader.num_files == 0:
                print('No training files are found.')
            else:
                train_data_subset = train_data_loader.get_minibatch(opt.stats_planets)
                train_data_dim = train_data_subset.size(1)
                print('Data row dim           : {:,}'.format(train_data_dim))
                print('Using parameters  dim  : {:,}'.format(params_dim))
                print('Using observation dim  : {:,}'.format(observation_dim))
                train_data_subset_observations = train_data_subset[:, params_dim+full_dim:params_dim+full_dim+observation_dim]  # observations
                print('Observation mean       : {}'.format(float(train_data_subset_observations.mean())))
                print('Observation median     : {}'.format(float(train_data_subset_observations.median())))
                print('Observation stddev     : {}'.format(float(train_data_subset_observations.std())))
                print('Log observation mean   : {}'.format(float(torch.log(train_data_subset_observations + epsilon).mean())))
                print('Log observation median : {}'.format(float(torch.log(train_data_subset_observations + epsilon).median())))
                print('Log observation stddev : {}'.format(float(torch.log(train_data_subset_observations + epsilon).std())))
            print()

            # Create the validation data loader
            print('Validation data')
            valid_data_loader = DataLoader(os.path.join(opt.root, opt.valid_dir), gcs_bucket, opt.max_blobs)
            if valid_data_loader.num_files == 0:
                print('No validation files are found.')
            else:
                valid_data_subset = valid_data_loader.get_minibatch(opt.stats_planets)
                valid_data_dim = valid_data_subset.size(1)
                print('Data row dim           : {:,}'.format(valid_data_dim))
                print('Using parameters  dim  : {:,}'.format(params_dim))
                print('Using observation dim  : {:,}'.format(observation_dim))
                valid_data_subset_observations = valid_data_subset[:, params_dim+full_dim:params_dim+full_dim+observation_dim]  # observations
                print('Observation mean       : {}'.format(float(valid_data_subset_observations.mean())))
                print('Observation median     : {}'.format(float(valid_data_subset_observations.median())))
                print('Observation stddev     : {}'.format(float(valid_data_subset_observations.std())))
                print('Log observation mean   : {}'.format(float(torch.log(valid_data_subset_observations + epsilon).mean())))
                print('Log observation median : {}'.format(float(torch.log(valid_data_subset_observations + epsilon).median())))
                print('Log observation stddev : {}'.format(float(torch.log(valid_data_subset_observations + epsilon).std())))
            print()

            print('\nChecking models in dir : {} ...'.format(opt.model_dir))
            if gcs_enabled:
                model_files = list(gcs_bucket.list_blobs(prefix=opt.model_dir))
                model_files = [os.path.basename(file.name) for file in model_files if '.model' in file.name]
            else:
                model_files = glob.glob(os.path.join(opt.root, os.path.join(opt.model_dir, '*.model')))
                model_files = [os.path.basename(file) for file in model_files]

            regex = r"inara_seed_(.*)_snapshot"
            matches = list(re.finditer(regex, '\n'.join(model_files), re.MULTILINE))
            seeds = [match.group(1) for match in matches]
            seeds = list(set(seeds))

            print('\nFound model files:')
            for model_file in model_files:
                print(model_file)

            print('\nFound random number seeds:')
            for seed in seeds:
                print(seed)

            print('\nIterating through model ensemble with {} seeds'.format(len(seeds)))
            for seed in seeds:
                print('\n** Loading latest model snapshot for seed: {} ...'.format(seed))
                model_file_name, model_dict = get_latest_model(device, os.path.join(opt.root, opt.model_dir), gcs_bucket, model_filename_prefix='inara_seed_'+str(seed))
                print('Loaded model info:')
                model_info = get_model_info(model_dict)
                print(model_info)

                if gcs_enabled:
                    print('Plotting to GCS bucket not implemented yet.')
                else:
                    plot_filename = os.path.join(os.path.join(opt.root, opt.model_dir), 'plots/' + os.path.basename(model_file_name).replace('.model', '.pdf'))
                    plot_dir = os.path.dirname(plot_filename)
                    if not os.path.exists(plot_dir):
                        print('Directory does not exists, creating: {} ...'.format(plot_dir))
                        os.makedirs(plot_dir)
                    print('Producing loss plot: {} ...'.format(plot_filename))

                    fig = plt.figure(figsize=(8, 6))
                    plt.plot(model_dict['train_loss_history_iter'], model_dict['train_loss_history'], label='Training')
                    plt.plot(model_dict['valid_loss_history_iter'], model_dict['valid_loss_history'], label='Validation')
                    plt.xlabel('Iteration')
                    plt.ylabel('Loss')
                    plt.legend(loc='upper right')
                    # plt.grid()
                    # plt.gcf().text(0, -0.01, 'test')
                    # plt.subplots_adjust(bottom=0.2)
                    fig.subplots_adjust(left=0.15, bottom=0.33, right=0.975, top=0.9)
                    plt.annotate('Type: {}\nModel params: {:,}\nTrain. iterations: {:,}\nMinibatch size: {:,}\nLearning rate: {:.6E}\nTrain. device: {}\nRandom seed: {}'.format(model_dict['model_type'], model_dict['num_params'], model_dict['iteration'], model_dict['minibatch_size'], model_dict['learning_rate'], model_dict['trained_on'], model_dict['seed']), (0, 0), (0, -40), xycoords='axes fraction', textcoords='offset points', va='top', family='monospace')
                    plt.annotate('Init. train. loss: {:.6E}\nFinal.train. loss: {:.6E}\nMin.  train. loss: {:.6E}\nInit. valid. loss: {:.6E}\nFinal.valid. loss: {:.6E}\nMin.  valid. loss: {:.6E}'.format(model_dict['train_loss_history'][0], model_dict['train_loss_history'][-1], model_dict['train_loss_min'], model_dict['valid_loss_history'][0], model_dict['valid_loss_history'][-1], model_dict['valid_loss_min']), (0.5, 0), (0, -40), xycoords='axes fraction', textcoords='offset points', va='top', family='monospace')
                    plt.title('INARA model: ' + os.path.basename(model_file_name))
                    # fig.tight_layout()
                    plt.savefig(plot_filename)

                    # model_dict = {'model': model, 'iteration': iteration, 'epoch': epoch, 'minibatch_size': opt.minibatch_size, 'train_loss_history': train_loss_history, 'train_loss_history_iter': train_loss_history_iter, 'valid_loss_history': valid_loss_history, 'valid_loss_history_iter': valid_loss_history_iter, 'train_loss_min': min_train_loss, 'valid_loss_min': min_valid_loss, 'seed': opt.seed, 'model_type': opt.train_model_type, 'num_params': num_params, 'trained_on': str(device)}

        elif opt.mode == 'createvm':
            print('\n*** CREATING VM ***\n')
            if opt.createvm_name is None or opt.createvm_command is None:
                raise RuntimeError('Expecting both createvm_name and createvm_command')

            print('Creating VM...')
            print('Name:    {}'.format(opt.createvm_name))
            print('Command: {}'.format(opt.createvm_command))
            createvm_command_list = opt.createvm_command.split(' ')
            command_container_command = '--container-command=' + createvm_command_list.pop(0)
            command_container_arg = ' '.join(list(map(lambda x:'--container-arg=' + x, createvm_command_list)))
            # Zones, in order of preference and based on requirements
            zones = ['us-west1-b', 'us-west1-c', 'us-west1-a', \
                     'us-west2-b', 'us-west2-c', 'us-west2-a', \
                     'us-central1-c', 'us-central1-f']
            iz = 0 # zone iteration attempt
            spawned = False # flag for spawning of machine
            while (spawned==False):
                try:
                    cmd = 'gcloud beta compute --project=astrobiology-team-2 instances create-with-container {} --zone {} --machine-type=n1-standard-4 --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=1072717194977-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/trace.append,https://www.googleapis.com/auth/devstorage.full_control --tags=http-server,https-server --image=cos-stable-67-10575-64-0 --image-project=cos-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=inara-template-1 --container-image=gcr.io/astrobiology-team-2/inara --container-restart-policy=never --container-tty {} {} --labels=container-vm=cos-stable-67-10575-64-0'.format(opt.createvm_name, zones[iz], command_container_command, command_container_arg)
                    print('Running command:')
                    print(cmd)
                    os.system(cmd)
                    print('Done.')
                    spawned = True
                except:
                    print('Zone resources full. Trying next zone.')
                    iz += 1
                    if iz==len(zones):
                        iz = 0

        elif opt.mode == 'createpsg':
            print('\n*** CREATING PSG ***\n')
            if opt.createpsg_name is None:
                raise RuntimeError('Expecting createpsg_name')

            print('Creating PSG VM...')
            print('Name:    {}'.format(opt.createpsg_name))
            cmd_1 = 'gcloud compute --project "astrobiology-team-2" disks create "{}" --size "500" --zone "us-west1-b" --source-snapshot "snapshot-psg-1" --type "pd-standard"'.format(opt.createpsg_name)
            print('Running command:')
            print(cmd_1)
            tmp_file = tempfile.NamedTemporaryFile()
            with open(tmp_file.name, 'w') as f:
                f.write(cmd_1)
            os.system('bash ' + tmp_file.name)

            cmd_2 = r"gcloud beta compute --project=astrobiology-team-2 instances create {} --zone=us-west1-b --machine-type=n1-highmem-16 --subnet=default --network-tier=PREMIUM --metadata=startup-script=\#\!\ /bin/bash$'\n'sudo\ /sbin/service\ httpd\ start$'\n'sudo\ /usr/sbin/setenforce\ Permissive,ssh-keys=psg:ssh-rsa\ AAAAB3NzaC1yc2EAAAADAQABAAABAQDQI4R7nynbRIALY2HRtRoqS1nwUZoSdQRfraPrxqXC0ecHFSHKGVMhG2NpGPdO9mI3MFEtTEqwWE/w1uRajYa5Mm8T3HRS5Dnl18ZN4QrczbwaCB9Qd9GE1b7a3aThfUnqRzXeBbtmSOSjaHrB2JkrEfk6Qu7pyOTAh8Yus/ladlb03bU8RmoODR6VXe2UDaQdntBv1d2dxhwGFfrNqMldKYpV\+730akEtj7arwr3pP/auHMolV6fIEbg5YKruge/JH1Z/oKid6iRKmLtGcBuEHJwA/h5xOWhlp\+jm9xOK1MgO3quOKF7D6QUKPubvoQEY/hIJjTJLaUZoopyfeEQ3\ psg$'\n'gpc:ssh-rsa\ AAAAB3NzaC1yc2EAAAADAQABAAABAQDFWQmUaYw1cmyzy7KI\+5lpt6zJPEtHm6l8uAFgy/HW\+NJDModOQ4yFI2mmqE2I0EpogOGkAMuyLrVNtKyInUPi0fUXpneCdWOUj2XM1IRLyRM33rAStG3t6\+qmi2krXMKpwaOSW45C/GC9KremV7ku7f6Wc6tIbZojvznOiOMWs54xCBoEjLFrF6u8w/tYMBVzQkJQ/puR79r1RfuBbn5JV5YfuDrYSmqBweyzRGQP9LS/VCTW3N2FTpKLSsj5AIE257l4EG9hv4fPJTuLLCH2O3avGkvHnlyu9xgoEgSQFrqIShqob3aOAQYSp8Eh4vxdKgrsxBFp\+t5Kox2id1tN\ gpc --maintenance-policy=MIGRATE --service-account=1072717194977-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --min-cpu-platform=Intel\ Skylake --tags=http-server,https-server --disk=name={},device-name={},mode=rw,boot=yes,auto-delete=yes".format(opt.createpsg_name, opt.createpsg_name, opt.createpsg_name)
            print('Running command:')
            print(cmd_2)
            with open(tmp_file.name, 'w') as f:
                f.write(cmd_2)
            os.system('bash ' + tmp_file.name)

            # os.system(cmd_2, )
            print('Done.')

        # Model training
        elif opt.mode == 'train':
            print('\n*** TRAINING MODE ***\n')
            if not gcs_enabled:
                full_path = os.path.join(opt.root, opt.model_dir)
                if not os.path.exists(full_path):
                    print('Directory does not exists, creating: {} ...'.format(full_path))
                    os.makedirs(full_path)
            # Create the training data loader
            train_data_loader = DataLoader(os.path.join(opt.root, opt.train_dir), gcs_bucket, opt.max_blobs)
            if train_data_loader.num_files == 0:
                raise RuntimeError('No training files are found.')
            # Create the validation data loader
            valid_data_loader = DataLoader(os.path.join(opt.root, opt.valid_dir), gcs_bucket, opt.max_blobs)
            if valid_data_loader.num_files == 0:
                raise RuntimeError('No validation files are found.')
            # Instatiation of the machine learning model
            print('\nCreating new model...')
            output_dim = params_slice[1] - params_slice[0]
            if opt.train_model_type == 'LR':
                print('Model type             : Linear regression')
                model = LinearRegression(input_dim=observation_dim, output_dim=output_dim)
            elif opt.train_model_type == 'FF':
                print('Model type             : Feed-forward neural network')
                model = FeedForwardNet1(input_dim=observation_dim, output_dim=output_dim)
            elif opt.train_model_type == 'FF2':
                print('Model type             : Feed-forward-2 neural network')
                model = FeedForwardNet2(input_dim=observation_dim, output_dim=output_dim)
            elif opt.train_model_type == 'CNN1':
                print('Model type             : Convolutional neural network 1')
                model = ConvNet1(input_dim=observation_dim, output_dim=output_dim)
            elif opt.train_model_type == 'CNN2':
                print('Model type             : Convolutional neural network 2')
                model = ConvNet2(input_dim=observation_dim, output_dim=output_dim)
            elif opt.train_model_type == 'CNN3':
                print('Model type             : Convolutional neural network 3')
                model = ConvNet3(input_dim=observation_dim, output_dim=output_dim)
            elif opt.train_model_type == 'CNN4':
                print('Model type             : Convolutional neural network 4')
                model = ConvNet4(input_dim=observation_dim, output_dim=output_dim)
            else:
                raise RuntimeError('Unexpected train_model_type: {}'.format(opt.train_model_type))

            # Move the model to GPU or CPU
            model.to(device)
            model.train()

            # Compute and report the number of trainable parameters in the model
            num_params = 0
            for p in model.parameters():
                num_params += p.nelement()
            print('Trainable parameters   : {:,}'.format(num_params))

            # Print model components
            print('Model components       :')
            print(list(model.modules())[0])

            # Setting the optimisation algorithm, learning rate, and weight_decay
            print('\nSelecting the optimisation algorithm...')
            output_dim = params_slice[1] - params_slice[0]
            if opt.optimiser == 'ADAM':
                print('Optimiser             : Adam')
                optimiser = optim.Adam(model.parameters(), lr=opt.learning_rate, weight_decay=opt.weight_decay)
            elif opt.optimiser == 'SGD':
                print('Optimiser             : Stochastic Gradient Descent')
                optimiser = optim.SGD(model.parameters(), lr=opt.learning_rate, momentum=0.9)
            elif opt.optimiser == 'ADA':
                print('Optimiser             : Adadelta')
                optimiser = optim.Adadelta(model.parameters(), lr=opt.learning_rate, rho=0.9, eps=1e-06, weight_decay=opt.weight_decay)
            elif opt.optimiser == 'RMS':
                print('Optimiser             : RMSprop by G. Hinton')
                optimiser = optim.RMSprop(model.parameters(), lr=opt.learning_rate, alpha=0.99, eps=1e-08, weight_decay=opt.weight_decay, momentum=0.1, centered=False)
            else:
                raise RuntimeError('Unexpected optimiser: {}'.format(opt.optimiser))

            # default values for initialising
            valid_loss = -1
            iteration = 0
            epoch = 1
            # Get the validation data
            print('Reading validation set...')
            valid_data = valid_data_loader.get_minibatch(opt.valid_size).to(device)
            print('Validation set ready.')
            valid_inputs = torch.log(valid_data[:, params_dim+full_dim:params_dim+full_dim+observation_dim] + epsilon)  # observations
            valid_inputs = (valid_inputs - opt.log_obs_mean) / opt.log_obs_stddev
            valid_targets = valid_data[:, :params_dim]  # target parameters
            valid_targets = valid_targets[:, params_slice[0]:params_slice[1]]
            valid_loss = float(F.mse_loss(model.forward(valid_inputs), valid_targets)/valid_data.size(0))  # average validation loss
            min_train_loss = sys.float_info.max
            min_valid_loss = valid_loss
            train_loss_history = []
            train_loss_history_iter = []
            valid_loss_history = [valid_loss]
            valid_loss_history_iter = [0]
            # Main training loop
            print()
            print('Epoch | Iter. | Train. loss  |Min.train.loss| Valid. loss  |Min.valid.loss| Planets/s')
            print('------|-------|--------------|--------------|--------------|--------------|----------')
            valid_loss_not_improved_count = 0
            time_start = time.time()
            while True:
                duration = time.time() - time_start
                planets_per_second = iteration * opt.minibatch_size / duration
                # Getting a minibatch and slice into inputs and outputs then pass it through the model
                print('Reading minibatch...', end='\r')
                sys.stdout.flush()
                minibatch = train_data_loader.get_minibatch(opt.minibatch_size).to(device)
                epoch = train_data_loader.epoch
                if epoch > opt.train_epochs:
                    print('Early stopping: train_epochs {} reached.'.format(opt.train_epochs))
                    break
                # To be commented
                inputs = torch.log(minibatch[:, params_dim+full_dim:params_dim+full_dim+observation_dim] + epsilon)  # observations
                inputs = (inputs - opt.log_obs_mean) / opt.log_obs_stddev
                targets = minibatch[:, :params_dim]  # target parameters
                targets = targets[:, params_slice[0]:params_slice[1]]
                optimiser.zero_grad()
                outputs = model.forward(inputs)  # generated parameters
                train_loss = F.mse_loss(outputs, targets)/opt.minibatch_size  # computing average training losss
                train_loss.backward()  # backpropapgation
                optimiser.step()  # optimisation step
                train_loss = float(train_loss)
                train_loss_history.append(train_loss)
                train_loss_history_iter.append(iteration)
                if train_loss < min_train_loss:
                    min_train_loss = train_loss
                # Validation section
                iteration += 1

                # Checks validation steps
                if iteration % opt.valid_iter == 0:
                    print('Validating...')
                    valid_loss = float(F.mse_loss(model.forward(valid_inputs), valid_targets)/valid_data.size(0))  # average validation loss
                    valid_loss_history.append(valid_loss)
                    valid_loss_history_iter.append(iteration)
                    if valid_loss < min_valid_loss:
                        min_valid_loss = valid_loss
                        valid_loss_not_improved_count = 0
                    else:
                        valid_loss_not_improved_count += 1
                        if valid_loss_not_improved_count > opt.train_early_stop_tolerance:
                            print('ABORT ABORT ABORT ! Early Stopping! No improvement to min. validation loss during last {} validations.'.format(opt.train_early_stop_tolerance))
                            break
                    # Saving the model
                    model_dict = {'model': model, 'iteration': iteration, 'epoch': epoch, 'minibatch_size': opt.minibatch_size, 'train_loss_history': train_loss_history, 'train_loss_history_iter': train_loss_history_iter, 'valid_loss_history': valid_loss_history, 'valid_loss_history_iter': valid_loss_history_iter, 'train_loss_min': min_train_loss, 'valid_loss_min': min_valid_loss, 'seed': opt.seed, 'model_type': opt.train_model_type, 'num_params': num_params, 'trained_on': str(device), 'learning_rate': opt.learning_rate}
                    model_filename = 'inara_seed_{}_snapshot_{}.model'.format(opt.seed, get_time_stamp())
                    model_full_path = os.path.join(opt.root, os.path.join(opt.model_dir, model_filename))
                    print('Saving model to file:', model_full_path)
                    # if Google Cloud Storage is enabled then save to bucket else save to local file system
                    if gcs_enabled:
                        tmp_file = tempfile.NamedTemporaryFile()
                        torch.save(model_dict, tmp_file)
                        tmp_file.seek(0)
                        blob = gcs_bucket.blob(os.path.join(opt.model_dir, os.path.basename(model_full_path)))
                        blob.upload_from_file(tmp_file)
                    else:
                        torch.save(model_dict, model_full_path)

                # if iteration % 10 == 0:
                print('{:5,} |{:6,} | {:.6E} | {:.6E} | {:.6E} | {:.6E} | {:,.2f}'.format(epoch, iteration, train_loss, min_train_loss, valid_loss, min_valid_loss, planets_per_second))

        # Use the trainied model to predict the parameters for a given observation
        elif opt.mode == 'test':
            print('\n*** TEST MODE ***\n')

            print('Checking test files in dir : {} ...'.format(opt.test_dir))
            if gcs_enabled:
                test_files = list(gcs_bucket.list_blobs(prefix=opt.test_dir))
                test_files = [os.path.basename(file.name) for file in test_files if 'csv' in file.name]
            else:
                test_files = glob.glob(os.path.join(opt.root, os.path.join(opt.test_dir, '*.csv')))
                if len(test_files) == 0:
                    test_files = glob.glob(os.path.join(opt.root, os.path.join(opt.test_dir, '*.npy')))

            # Filtering out any test output files in the subdirectories that may be remaining from a previous test execution. We don't want to treat them as test input files.
            test_files = [file for file in test_files if 'output' not in file]

            if len(test_files) == 0:
                print('No files found in test_dir: {}'.format(opt.test_dir))
                sys.exit(1)

            print('\nFound test files:')
            for test_file in test_files:
                print(test_file)

            print('\nChecking models in dir : {} ...'.format(opt.model_dir))
            if gcs_enabled:
                model_files = list(gcs_bucket.list_blobs(prefix=opt.model_dir))
                model_files = [os.path.basename(file.name) for file in model_files if '.model' in file.name]
            else:
                model_files = glob.glob(os.path.join(opt.root, os.path.join(opt.model_dir, '*.model')))

            regex = r"inara_seed_(.*)_snapshot"
            matches = list(re.finditer(regex, '\n'.join(model_files), re.MULTILINE))
            seeds = [match.group(1) for match in matches]
            seeds = list(set(seeds))

            print('\nFound model files:')
            for model_file in model_files:
                print(model_file)

            print('\nFound random number seeds:')
            for seed in seeds:
                print(seed)

            print('\nIterating through model ensemble with {} seeds'.format(len(seeds)))
            for seed in seeds:
                print('\n** Loading latest model snapshot for seed: {} ...'.format(seed))
                _, model_dict = get_latest_model(device, os.path.join(opt.root, opt.model_dir), gcs_bucket, model_filename_prefix='inara_seed_'+str(seed))
                print('Loaded model info:')
                model_info = get_model_info(model_dict)
                print(model_info)
                model = model_dict['model']
                model.to(device)
                # Set the model in evaluation mode
                #model.eval() <====================commented out by Frank for the dropout training with CNN4

                for test_file in test_files:
                    print('\nReading test input file: {} ...'.format(test_file))
                    if gcs_enabled:
                        test_rows = DataLoader.read_data_from_blob(gcs_bucket.get_blob(os.path.join(opt.test_dir, test_file)))
                    else:
                        test_rows = DataLoader.read_data_from_file(os.path.join(opt.root, os.path.join(opt.test_dir, test_file)))
                    test_input = torch.log(test_rows[:, params_dim+full_dim:params_dim+full_dim+observation_dim] + epsilon)
                    test_input = (test_input - opt.log_obs_mean) / opt.log_obs_stddev
                    test_input = test_input.to(device)
                    # Run the neural network, get test output
                    print('Running model...')
                    print('THE SHAPE OF THE TEST_INPUT IS :', test_input.shape)
                    test_output = model.forward(test_input)
                    print('THE SHAPE OF THE TEST_OUTPUT IS :', test_output.shape)

                    if 'csv' in test_file:
                        test_output_file_name = test_file.replace('.csv', '') + '_output_seed_{}_execution_{}.csv'.format(seed, uuid.uuid4())
                    else:
                        test_output_file_name = test_file.replace('.npy', '') + '_output_seed_{}_execution_{}.npy'.format(seed, uuid.uuid4())
                    test_output_filename_full_path = os.path.join(opt.test_dir, os.path.join('predictions', test_output_file_name))
                    print('Saving test output file: {} ...'.format(test_output_filename_full_path))
                    if gcs_enabled:
                        blob = gcs_bucket.blob(test_output_filename_full_path)
                        DataLoader.write_data_to_blob(test_output, blob)
                    else:
                        DataLoader.write_data_to_file(test_output, test_output_filename_full_path)

                    test_plot_file_name = test_output_file_name.replace('.npy', '.pdf')
                    test_ground_truth_params = test_rows[:, :params_dim]
                    test_ground_truth_params = test_ground_truth_params[:, params_slice[0]:params_slice[1]]
                    print()
                    print(test_output)
                    print(test_ground_truth_params)
                    # fig = plt.figure(figsize=(8, 6))
                    # plt.plot(model_dict['train_loss_history_iter'], model_dict['train_loss_history'], label='Training')
                    # plt.plot(model_dict['valid_loss_history_iter'], model_dict['valid_loss_history'], label='Validation')
                    # plt.xlabel('Iteration')
                    # plt.ylabel('Loss')
                    # plt.legend(loc='upper right')
                    # # plt.grid()
                    # # plt.gcf().text(0, -0.01, 'test')
                    # # plt.subplots_adjust(bottom=0.2)
                    # fig.subplots_adjust(left=0.15, bottom=0.33, right=0.975, top=0.9)
                    # plt.annotate('Type: {}\nModel params: {:,}\nTrain. iterations: {:,}\nMinibatch size: {:,}\nLearning rate: {:.6E}\nTrain. device: {}\nRandom seed: {}'.format(model_dict['model_type'], model_dict['num_params'], model_dict['iteration'], model_dict['minibatch_size'], model_dict['learning_rate'], model_dict['trained_on'], model_dict['seed']), (0, 0), (0, -40), xycoords='axes fraction', textcoords='offset points', va='top', family='monospace')
                    # plt.annotate('Init. train. loss: {:.6E}\nFinal.train. loss: {:.6E}\nMin.  train. loss: {:.6E}\nInit. valid. loss: {:.6E}\nFinal.valid. loss: {:.6E}\nMin.  valid. loss: {:.6E}'.format(model_dict['train_loss_history'][0], model_dict['train_loss_history'][-1], model_dict['train_loss_min'], model_dict['valid_loss_history'][0], model_dict['valid_loss_history'][-1], model_dict['valid_loss_min']), (0.5, 0), (0, -40), xycoords='axes fraction', textcoords='offset points', va='top', family='monospace')
                    # plt.title('INARA model: ' + os.path.basename(model_file_name))
                    # # fig.tight_layout()
                    # plt.savefig(plot_filename)
        else:
            raise RuntimeError('Unknown mode.')

    except KeyboardInterrupt:
        print('Stopped.')
    except Exception:
        traceback.print_exc(file=sys.stdout)


if __name__ == '__main__':
    main()
