# NASA FDL 2018 Astrobiology Team 2
# INARA: Intelligent exoplaNet Atmosphere RetrievAl
#
# July 2018
# ensemble_nn.py

import os
import sys

N = 1

os.system('python run.py --mode datagen --datagen_files 1 --datagen_planets_per_file 3 --gentest 1 --root gs://inara-debug-7 --datagen_dir test --psg_url http://35.231.69.180/api.php')

for n in range(N):
    # Train N models
    os.system('python run.py --root gs://inara-debug-7 --train_epochs 1 --seed ' + str(n) + ' --mode train --model_dir model_ensemble/'+str(n)+' --cuda --valid_iter 10')
    # Test N models
    os.system('python run.py --root gs://inara-debug-7 --mode test --model_dir model_ensemble/'+str(n)+' --cuda --test_file test/test.csv --test_out_file test/output/test'+str(n)+'.csv')

#os.sys.exit(0)

sys.exit(1)
