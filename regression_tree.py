
# coding: utf-8

import os
import numpy as np
# import torch
# import pandas as pd
from io import StringIO, BytesIO
from google.cloud import storage
# import matplotlib.pyplot as plt#, mpld3
from sklearn import ensemble
from sklearn.preprocessing import MinMaxScaler
from run import DataLoader
# from tensorflow.python.lib.io import file_io
import csv
# get_ipython().run_line_magic('matplotlib', 'inline')
# os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="../ab2.json"


# In[2]:


bucket_name = 'inara-debug-11'
gcs_storage_client = storage.Client()
print('Current buckets in the account:')
# List all the available buckets
for bucket in list(gcs_storage_client.list_buckets()):
    print(' ' + bucket.name)
print('Setting root bucket: {} ...'.format(bucket_name))
gcs_bucket = gcs_storage_client.get_bucket(bucket_name)

def read_data_from_blob(blob):
    # print(blob.download_as_string()[0])
    data_string = blob.download_as_string().decode('utf-8','ignore')
        # Creating a torch tensor here from a single blob (csv format)
    # import pdb;
    return np.load(BytesIO(data_string))
    # return np.loadtxt(StringIO(data_string),delimiter='\t')

dir = ''
file_list = list(gcs_bucket.list_blobs(prefix=dir))
print(file_list[1])

num_inputs = 11515
num_outputs = 11
num_examples = 1000



data = read_data_from_blob(file_list[0])
print('size of data from one file:'+str(len(data)))

Nt = 2 #len(file_list)
print('Number of files for training:: ',len(file_list))
# Nt = 30
for i in range(1,int(Nt)):
    print('Adding file number '+str(i))
    data = np.hstack((data, read_data_from_file(files[i])))
    # np.save(file_io.FileIO('gs://'+bucket_name+'/pred/train_x_'+str(Nt)+'_np.npy', 'w'), np.array(data))
    
# np.array(data).shape


# In[3]:


params_dim = 28
observation_dim = 4379 # observation size, no clouds, final decision data size
full_dim = 15346

obs_index_s = params_dim+full_dim
obs_index_e = params_dim+full_dim+observation_dim
# noise_ind = params_dim+2*full_dim:params_dim+2*full_dim+observation_dim
x_data = data[:,obs_index_s:obs_index_e] # Observations (No noise) 
y_data = data[:,13:25]
# y_data2 = data[:,1:3]
# print(np.hstack([y_data, y_data2]).shape)
# y_data = np.hstack([y_data, y_data2])

Ntr = 15000
x_data_tr = x_data[:Ntr]
y_data_tr = y_data[:Ntr]

x_data_val = x_data[Ntr:]
y_data_val = y_data[Ntr:]



# https://github.com/exoclime/HELA/blob/ffe2f0e17c86c8b593eaec4a050ec610865e36d1/example_dataset/example_dataset.json


class Model:
    
    def __init__(self, num_trees, num_jobs,ranges, verbose=1):
        scaler = MinMaxScaler(feature_range=(0, 100))
        rf = ensemble.RandomForestRegressor(n_estimators=num_trees,
                                            oob_score=True,
                                            verbose=verbose,
                                            n_jobs=num_jobs,
                                            max_features="sqrt",
                                            min_impurity_decrease=0.01)
        self.scaler = scaler
        self.rf = rf
        self.num_trees = num_trees
        self.num_jobs = num_jobs
        self.verbose = verbose
        self.ranges = ranges
    
    def _scaler_fit(self, y):
        if y.ndim == 1:
            y = y[:, None]
        
        self.scaler.fit(y)
    
    def _scaler_transform(self, y):
        if y.ndim == 1:
            y = y[:, None]
            return self.scaler.transform(y)[:, 0]
        
        return self.scaler.transform(y)
    
    def _scaler_inverse_transform(self, y):
        
        if y.ndim == 1:
            y = y[:, None]
            return self.scaler.inverse_transform(y)[:, 0]
        
        return self.scaler.inverse_transform(y)
    
    def fit(self, x, y):
        self._scaler_fit(y)
        self.rf.fit(x, self._scaler_transform(y))
    
    def predict(self, x):
        pred = self.rf.predict(x)
        return self._scaler_inverse_transform(pred)
    
#     def get_params(self, deep=True):
#         return {"num_trees": self.num_trees, "num_jobs": self.num_jobs,
#                 "names": self.names, "ranges": self.ranges,
#                 "colors": self.colors,
#                 "verbose": self.verbose}
    
    def trees_predict(self, x):
        
        if x.ndim > 1:
            raise ValueError("x.ndim must be 1")
        
        preds = np.array([i.predict(x[None, :])[0] for i in self.rf.estimators_])
        return self._scaler_inverse_transform(preds)



def train_model(training_x, training_y, num_trees=1000, num_jobs=10, verbose=1):
#     pipeline = Model(num_trees, num_jobs,
#                         names=dataset.names,
#                         ranges=dataset.ranges, [[0, 3000], [-13, 0], [-13, 0], [-13, 0], [-13, 0]]
#                         colors=dataset.colors,
#                         verbose=verbose)
    pipeline = Model(num_trees, num_jobs,
                    ranges=[[0, 1],[0, 1],[0, 1],[0, 1],[0, 1],[0, 1],[0, 1],[0, 1],[0, 1],[0, 1],[0, 1],[0, 1]],
                    verbose=verbose)
    pipeline.fit(training_x, training_y)
    return pipeline


def test_model(model, dataset, output_path):
    
    if dataset.testing_x is None:
        return
    
    logger.info("Testing model...")
    pred = model.predict(dataset.testing_x)
    r2scores = {name_i: metrics.r2_score(real_i, pred_i)
                    for name_i, real_i, pred_i in zip(dataset.names, dataset.testing_y.T, pred.T)}
    print("Testing scores:")
    for name, values in r2scores.items():
        print("\tR^2 score for {}: {:.3f}".format(name, values))
    
    logger.info("Plotting testing results...")
    fig = plot.predicted_vs_real(dataset.testing_y, pred, dataset.names, dataset.ranges)
    fig.savefig(os.path.join(output_path, "predicted_vs_real.pdf"),
                bbox_inches='tight')


def compute_feature_importance(model, dataset, output_path):
    
    logger.info("Computing feature importance for individual parameters...")
    regr = multioutput.MultiOutputRegressor(model, n_jobs=1)
    regr.fit(dataset.training_x, dataset.training_y)
    
    fig = plot.feature_importances(forests=[i.rf for i in regr.estimators_] + [model.rf],
                                   names=dataset.names + ["joint prediction"],
                                   colors=dataset.colors + ["C0"])
    
    fig.savefig(os.path.join(output_path, "feature_importances.pdf"),
                bbox_inches='tight')


def prediction_ranges(preds):
    
    percentiles = (np.percentile(pred_i, [50, 16, 84]) for pred_i in preds.T)
    return np.array([(a, c-a, a-b) for a, b, c in percentiles])


def main_train(training_dataset, model_path,
               num_trees, num_jobs,
               feature_importance, quiet,
               **kwargs):
    
    logger.info("Loading dataset '{}'...".format(training_dataset))
    dataset = load_dataset(training_dataset)
    
    logger.info("Training model...")
    model = train_model(dataset, num_trees, num_jobs, not quiet)
    
    maybe_makedirs(model_path)
    model_file = os.path.join(model_path, "model.pkl")
    logger.info("Saving model to '{}'...".format(model_file))
    joblib.dump(model, model_file)
    
    logger.info("Printing model information...")
    print("OOB score: {:.4f}".format(model.rf.oob_score_))
    
    test_model(model, dataset, model_path)
    
    if feature_importance:
        compute_feature_importance(model, dataset, model_path)


def main_predict(model_path, data_file, output_path, plot_posterior, **kwargs):
    
    model_file = os.path.join(model_path, "model.pkl")
    logger.info("Loading random forest from '{}'...".format(model_file))
    model = joblib.load(model_file)
    
    logger.info("Loading data from '{}'...".format(data_file))
    data, _ = load_data_file(data_file, model.rf.n_features_)
    
    preds = model.trees_predict(data[0])
    pred_ranges = prediction_ranges(preds)
    
    for name_i, pred_range_i in zip(model.names, pred_ranges):
        print("Prediction for {}: {:.3g} [+{:.3g} -{:.3g}]".format(name_i, *pred_range_i))
    
    if plot_posterior:
        logger.info("Plotting and saving the posterior matrix...")
        fig = plot.posterior_matrix(preds, None,
                                    names=model.names,
                                    ranges=model.ranges,
                                    colors=model.colors)
        maybe_makedirs(output_path)
        fig.savefig(os.path.join(output_path, "posterior_matrix.pdf"),
                    bbox_inches='tight')


def show_usage(parser, **kwargs):
    parser.print_help()


# In[9]:


pipeline = train_model(training_x=np.array(x_data_tr), training_y=np.array(y_data_tr))


# In[10]:


# predictions = pipeline.trees_predict(np.array(x_data)[0,:])



# ### Generate Test Data
# print('Generate valid data')
# # In[15]:

# bucket_name = 'inara-debug-10'
# gcs_storage_client = storage.Client()
# print('Current buckets in the account:')
# # List all the available buckets
# for bucket in list(gcs_storage_client.list_buckets()):
#     print(' ' + bucket.name)
# print('Setting root bucket: {} ...'.format(bucket_name))
# gcs_bucket = gcs_storage_client.get_bucket(bucket_name)

# def read_data_from_blob(blob):
#         data_string = blob.download_as_string().decode('utf-8')
#         # Creating a torch tensor here from a single blob (csv format)
#         return pd.read_csv(StringIO(data_string), delimiter='\t', header=None)

# dir = 'valid'
# file_list = list(gcs_bucket.list_blobs(prefix=dir))
# print(file_list[1])

# num_inputs = 11515
# num_outputs = 11

# data_valid = read_data_from_blob(file_list[1])
# print('size of data from one file:'+str(len(data_valid)))

# Nv = len(file_list)
# print('Number of files for valid:: ',len(file_list))
# # Nv = 30
# for i in range(2,int(Nv)+1):
#     print('Adding file number '+str(i))
#     data_valid = pd.concat([data_valid, read_data_from_blob(file_list[i])])
    
# np.array(data_valid).shape


# In[16]:


# x_data_valid = data_valid.iloc[:,obs_index_s:obs_index_e]
# y_data_valid = data_valid.iloc[:,13:25]
# y_data2_valid = data_valid.iloc[:,1:3]
# print(np.hstack([y_data_valid, y_data2_valid]).shape)
# y_data_valid = np.hstack([y_data_valid, y_data2_valid])


# In[17]:


# predictions_valid = pipeline.trees_predict(np.array(x_data_valid)[0,:])


# # In[18]:


# plt.scatter(predictions_valid[:,0],predictions_valid[:,1])
# plt.xlabel('s')
# plt.xlabel('s')
# plt.show()

PRED = np.zeros((y_data_val.shape[0],1000,14))
for planet in range(y_data_val.shape[0]):
    PRED[planet] = pipeline.trees_predict(np.array(x_data_val)[planet,:])
# predictions_valid = pipeline.trees_predict(np.array(x_data_valid)[0,:])
# test_output_file_name = 'gs://' + bucket_name
# test_output_filename_full_path = os.path.join('test', os.path.join('predictions_tree', test_output_file_name))
# import pdb; pdb.set_trace()
np.save('PRED_5000.npy', PRED)
np.save('y_val_5000.npy', y_data_val)
np.save('gs://'+bucket_name+'/pred/valid_tree'+str(Nt)+'.npy', y_data_val)
np.save('gs://'+bucket_name+'/pred/predictions_valid_tree'+str(Nt)+'.npy', PRED)