import sys
import numpy as np
import scipy.stats as ss
import scipy.interpolate as si
import astropy.constants as const
from . import PT
from . import obs
import pypsg

def genPT(log10kappa, log10gamma1, log10gamma2, alpha, beta, pmin, pmax, Rstar,
          Tstar, sma, surfgrav, Tint=0, nlayers=50):
    """
    This function generatures a pressure-temperature (PT) profile for given
    input parameters using the model of Line et al. (2013).

    Note that this uses PT.py and reader.py from BART, the Bayesian Atmospheric
    Radiative Transfer code, which has an open-source, reproducible-research license.


    Inputs
    ------
    log10kappa:   Planck thermal IR opacity (in units cm^2/gr)
    log10gamma1:  Visible-to-thermal stream Planck mean opacity ratio.
    log10gamma2:  Visible-to-thermal stream Planck mean opacity ratio.
    alpha:         Visible-stream partition (0.0--1.0).
    beta:          A 'catch-all' for albedo, emissivity, and day-night
                      redistribution (on the order of unity).
    pmin:          Minimum pressure at the top of the atmosphere (in bars).
    pmax:          Pressure at the surface of the planet (in bars).
    Rstar:         Radius of the host star (in solar radii).
    Tstar:         Temperature of the host star (in Kelvin).
    sma:           Semimajor axis of planet (in AU).
    surfgrav:      Planetary gravity at 1 bar of pressure (in cm/s^2).
    Tint:          Planetary internal heat flux (in Kelvin).
                   Default is 0 since these planets will have negligible flux
                   compared to host star's incident flux.
    nlayers:       Number of layers in the atmosphere.

    Returns
    -------
    PTprof:        2D array containing the pressure (equally spaced in
                   logspace) and temperature at each pressure.
                   PTprof[i] gives the i-th layer's [pressure, temperature]
                   PTprof[:,0] gives the pressure at all layers.
                   PTprof[:,1] gives the temperature at all layers.
    """
    # Put PT parameters into array
    params   = np.array([log10kappa, log10gamma1, log10gamma2, alpha, beta])
    # Range of pressures equally spaced in logspace
    pressure = np.logspace(np.log10(pmax), np.log10(pmin), nlayers)

    # Generate temperature profile
    #                  [bars]   [params]        [meters]            [K]
    #                  [K]      [meters]        [cm/s^2]
    temp = PT.PT_line(pressure, params, Rstar * const.R_sun.value, Tstar,
                      Tint, sma * const.au.value, surfgrav)

    # Put temperature and pressure into array, return
    PTprof      = np.zeros((nlayers, 2), dtype=float)
    PTprof[:,0] = pressure
    PTprof[:,1] = temp

    return PTprof


def makeclouds(abun, PTprof, nT=400):
    """
    This function computes the amount of condensation that will occur for a species.
    Note that clouds are only computed within the range of our data
    Allowed species: H2O (255.9 K <= T <= 573 K), NH3 (164 K <= T <= 371.5 K)
    Sources:
    H2O: https://webbook.nist.gov/cgi/cbook.cgi?ID=C7732185&Units=SI&Mask=4#Thermo-Phase
    NH3: https://webbook.nist.gov/cgi/cbook.cgi?ID=C7664417&Units=SI&Mask=4#Thermo-Phase

    Inputs
    ------
    abun    - Abundance of H2O and NH3.
    PTprof  - PT profile as created by genPT().
    nT      - Int. Number of points to calculate SVP.
    
    Outputs
    -------
    clouds  - Abundance of H2O and NH3 clouds at each layer in the atmosphere.
    """
    # Repeat abundance array to match PTprof shape
    #abunarr = np.repeat(abun, len(PTprof[:,0]))

    # Arrays of Antoine equation parameters for Saturation Vapor Pressure (SVP)
    #                Tlo,   Thi,  A,       B,          C
    h2o = np.array([[255.9, 373. , 4.6543 , 1435.264,  -64.848], 
                    [273. , 303. , 5.40221, 1838.675,  -31.737], 
                    [304. , 333. , 5.20389, 1733.926,  -39.485], 
                    [334. , 363. , 5.0768 , 1659.793,  -45.854], 
                    [344. , 373. , 5.08354, 1663.125,  -45.622], 
                    [379. , 573. , 3.55959,  643.748, -198.043]])
    nh3 = np.array([[164. , 239.6, 3.18757,  506.713,  -80.78 ], 
                    [239.6, 371.5, 4.86886, 1113.928,  -10.409]])

    Th2o = np.linspace(np.amin(h2o[:,0]), np.amax(h2o[:,1]), num=nT)
    Tnh3 = np.linspace(np.amin(nh3[:,0]), np.amax(h2o[:,1]), num=nT)

    # Use Antoine equation to calculate SVP
    # p[bar] = 10^(A - (B / (T[K] + C)))
    svph2o = np.zeros(nT)
    svpnh3 = np.zeros(nT)

    # Different temperature regimes for H2O
    ir1 =  Th2o < h2o[1,0]
    ir2 = (Th2o >= h2o[1,0])              * (Th2o <= (h2o[2,0]+h2o[1,1])/2.)
    ir3 = (Th2o > (h2o[2,0]+h2o[1,1])/2.) * (Th2o <= (h2o[3,0]+h2o[2,1])/2.)
    ir4 = (Th2o > (h2o[3,0]+h2o[2,1])/2.) * (Th2o <   h2o[4,0])
    ir5 = (Th2o >= h2o[4,0]) * (Th2o<=h2o[3,1])
    ir6 = (Th2o >  h2o[3,1]) * (Th2o<=h2o[4,1])
    ir7 =  Th2o >= h2o[5,0]
    # Fill in SVP for H2O
    svph2o[ir1] =  10**(h2o[0,2] - (h2o[0,3] / (Th2o[ir1] + h2o[0,4])))
    svph2o[ir2] =  10**(h2o[1,2] - (h2o[1,3] / (Th2o[ir2] + h2o[1,4])))
    svph2o[ir3] =  10**(h2o[2,2] - (h2o[2,3] / (Th2o[ir3] + h2o[2,4])))
    svph2o[ir4] =  10**(h2o[3,2] - (h2o[3,3] / (Th2o[ir4] + h2o[3,4])))
    svph2o[ir5] = (10**(h2o[3,2] - (h2o[3,3] / (Th2o[ir5] + h2o[3,4]))) + \
                   10**(h2o[4,2] - (h2o[4,3] / (Th2o[ir5] + h2o[4,4])))) / 2.
    svph2o[ir6] =  10**(h2o[4,2] - (h2o[4,3] / (Th2o[ir6] + h2o[4,4])))
    svph2o[ir7] =  10**(h2o[5,2] - (h2o[5,3] / (Th2o[ir7] + h2o[5,4])))

    # Different temperature regimes for NH3
    ir1 = Tnh3 <= nh3[0,1]
    ir2 = Tnh3 >  nh3[1,0]
    # Fill in SVP for NH3
    svpnh3[ir1] = 10**(nh3[0,2] - (nh3[0,3] / (Tnh3[ir1] + nh3[0,4])))
    svpnh3[ir2] = 10**(nh3[1,2] - (nh3[1,3] / (Tnh3[ir2] + nh3[1,4])))

    # Remove the 0 values as they will mess up interpolation
    Th2o   =   Th2o[svph2o!=0]
    Tnh3   =   Tnh3[svpnh3!=0]
    svph2o = svph2o[svph2o!=0]
    svpnh3 = svpnh3[svpnh3!=0]

    # Functions and indices for interpolation
    # We only allow interpolation where the data is defined
    h2osvpinterp = si.interp1d(Th2o, svph2o)
    ih2o         = (PTprof[:,1] >= Th2o[0]) * (PTprof[:,1] <= Th2o[-1])
    nh3svpinterp = si.interp1d(Tnh3, svpnh3)
    inh3         = (PTprof[:,1] >= Tnh3[0]) * (PTprof[:,1] <= Tnh3[-1])
    # Interpolate to the desired values
    laysvp          = np.zeros((len(PTprof[:,1]), 2))
    laysvp[ih2o, 0] = h2osvpinterp(PTprof[:,1][ih2o])
    laysvp[inh3, 1] = nh3svpinterp(PTprof[:,1][inh3])

    # Arrays to hold cloud info
    clouds    = np.zeros((len(PTprof[:,0]), 2))
    cloudsh2o = np.zeros( len(PTprof[:,0]))
    cloudsnh3 = np.zeros( len(PTprof[:,0]))
    # Indices to determine where SVP is defined
    ih2o = laysvp[:,0]!=0
    inh3 = laysvp[:,1]!=0

    # Calculate cloud abundances
    cloudsh2o[ih2o] = abun[0] * ((abun[0] * PTprof[:,0])[ih2o] - laysvp[ih2o,0]) / \
                                 (abun[0] * PTprof[:,0])[ih2o]
    cloudsnh3[inh3] = abun[1] * ((abun[1] * PTprof[:,0])[inh3] - laysvp[inh3,1]) / \
                                 (abun[1] * PTprof[:,0])[inh3]

    # Package array, remove negatives
    clouds[:,0] = cloudsh2o
    clouds[:,1] = cloudsnh3
    clouds[clouds<0] = 0

    return clouds


def genparams(obstype, pmin=1e-6):
    """
    This function samples parameters within a given range. Some parameters
    influence others. A summary of the method:

    Inputs
    ------
    obstype     - string. 'do' for direct  observations (F, G, K types), 
                          'tr' for transit observations (M types).
    pmin        - Minimum pressure to consider in atmospheric model. [bars]
                  Default is roughly the bottom of thermosphere for Earth.

    Returns
    -------
    starclass   - Stellar class. (F, G, K, or M)
    Tstar       - Stellar temperature. [K]
    Rstar       - Stellar radius. [R_sun]
    sma         - Semimajor axis of planet. [AU]
    rplanet     - Planetary radius. [R_earth]
    density     - Planetary density. [g/cm^3]
    surfpres    - Planetary 'surface' pressure. [bars]
    surfgrav    - Planetary surface gravity. [cm/s^2]
    surftemp    - Planetary 'surface' temperature. [K]
    mols        - Molecules in the atmosphere.
    abuns       - Abundances of each molecule in the atmosphere given in `mols'.
    kappa       - Planck thermal IR opacity [cm^2/g]
    gamma1      - Visible-to-thermal stream Planck mean opacity ratio.
    gamma2      - Visible-to-thermal stream Planck mean opacity ratio.
    alpha       - Visible-stream partition (0.0--1.0).
    beta        - A 'catch-all' for albedo, emissivity, and day-night
                  redistribution (on the order of unity).
    PTprof      - Pressure-temperature profile.
    surftemp    - Planetary surface temperature. [K]
    albedo      - Planetary albedo.
    """
    # Stellar classes we are considering
    if   obstype=='do':
        sclass=['U', 'G', 'K'] #'U' is F-type, using blackbody model
        # Generate a star class
        starclass = sclass[np.random.randint(0, len(sclass))]
    elif obstype=='tr':
        starclass = 'M'
    else:
        print("Error specifying observation type. Please choose 'do' or 'tr'.")
        sys.exit()
    # Generate a temperature and radius for the star
    # Source for temps: https://www.cfa.harvard.edu/~pberlind/atlas/htmls/note.html
    # Source for radii, and also temps, F, G, K: https://arxiv.org/pdf/1306.2974.pdf
    # ^ Boyajian et al. 2013, ApJ, "Stellar Diameters and Temperatures III. ..."
    # Source for radii, temps, K, M: https://arxiv.org/pdf/1208.2431.pdf
    # ^ Boyajian et al, 2012, ApJ, "Stellar Diameters and Temperatures II. ..."
    if starclass=='U': # This is F-type -- 'U' uses blackbody model
        # Note upper bound on F-type temp is 7200 due to limitations of
        # Kopparapu et al 2013 referenced below for SMA range approximation
        Tstar = np.random.uniform(5900., 7200.)
        if Tstar > 6400:
            Rstar = np.random.uniform(1.40, 2.00)
        else:
            Rstar = np.random.uniform(1.10, 2.00)
    elif starclass=='G':
        Tstar = np.random.uniform(5300., 5900.)
        if Tstar > 5500:
            Rstar = np.random.uniform(0.90, 1.30)
        else:
            Rstar = np.random.uniform(0.80, 1.10)
    elif starclass=='K':
        Tstar = np.random.uniform(3800., 5300.)
        if Tstar > 5000:
            Rstar = np.random.uniform(0.70, 0.95)
        else:
            Rstar = np.random.uniform(0.60, 0.85)
    elif starclass=='M':
        Tstar = np.random.uniform(3000., 3800.)
        if Tstar < 3250:
            Rstar = np.random.uniform(0.14, 0.40)
        else:
            Rstar = np.random.uniform(0.30, 0.55)
    else:
        print("Something went horribly wrong with choosing a stellar class.\n")
        sys.exit()

    if starclass=='M':
        dist = np.random.uniform(5. , 25.)
    else:
        dist = np.random.uniform(1.3, 15.)

    # Calculate stellar luminosity
    Lstar = const.sigma_sb.value * \
            4. * np.pi * (Rstar * const.R_sun.value)**2 * Tstar**4

    # Use Tstar to find the bounds on the semimajor axis
    # Approximation source: Kopparapu et al 2013, ApJ,
    # "Habitable Zones Around Main-sequence Stars: New Estimates"
    # S_eff = S_eff0 + aT_star + bT_star^2 + cT_star^3 + dT_star^4
    Slo = 1.7763 + 1.4335e-04 * (Tstar - 5780.)    + 3.3954e-09 * (Tstar - 5780.)**2 - \
                   7.6364e-12 * (Tstar - 5780.)**3 - 1.1950e-15 * (Tstar - 5780.)**4
    Shi = 0.3207 + 5.4471e-05 * (Tstar - 5780.)    + 1.5275e-09 * (Tstar - 5780.)**2 - \
                   2.1709e-12 * (Tstar - 5780.)**3 - 3.8282e-16 * (Tstar - 5780.)**4
    # Convert S values to semimajor axis [AU]
    smalo = (Lstar / const.L_sun.value / Slo)**0.5
    smahi = (Lstar / const.L_sun.value / Shi)**0.5
    # Generate a semimajor axis [AU]
    sma = np.random.uniform(smalo, smahi)

    # Generate a planet that can hold onto an atmosphere
    canholdatmo = False
    while(canholdatmo == False):
        # Generate a planetary radius [R_earth]
        # Mars to around the upper limit on rocky planets
        rplanet = np.random.uniform(0.5, 1.6)
        # Calcuate mass from radius using relation from
        # Sotin et al 2007, Icarus, 
        # "Mass-radius curve for extrasolar Earth-like planets and ocean planets"
        # Including a factor of +- 2% to account for the over/under shoot in paper
        if rplanet <= 1.:
            mplanet = (rplanet / 1.)**(1./0.306) * np.random.uniform(0.98, 1.02)
        elif rplanet > 1.:
            mplanet = (rplanet / 1.)**(1./0.274) * np.random.uniform(0.98, 1.02)
        else:
            print("Something went horribly wrong with calculating the mass.\n")
            sys.exit()

        # Calculate density [g/cm^3], surface gravity [cm/s^2]
        density  = mplanet * const.M_earth.cgs.value /                      \
                   (4./3. * np.pi * (rplanet * const.R_earth.cgs.value)**3)
        surfgrav = const.G.cgs.value * mplanet * const.M_earth.cgs.value /  \
                   (rplanet * const.R_earth.cgs.value)**2

        # Calculate surface escape velocity [km/s^2]
        vesc = (2. * const.G.value * mplanet * const.M_earth.value / \
                (rplanet * const.R_earth.value))**0.5                 / 1000.

        # Calculte planet's insolation Insol, relative to Earth
        Insol = Lstar / const.L_sun.value / sma**2

        # Rough approx of 'cosmic shoreline' eqn in Zahnle & Catling 2016
        # 1e-6 Insol : 0.2 vesc, 1e4 Insol : 70 vesc -- straight line on loglog
        # Slope of cosmic shoreline
        shoreline = np.log10(1e4 / 1e-6) / np.log10(70. / 0.2)

        # Calculate shoreline insolation, pinsol, value for the planet's vesc 
        # to see if it can hold an atmosphere
        # Requires the actual insolation, Insol, to be less than pinsol
        pinsol = 1e4 * (vesc / 70.)**shoreline
        if Insol < pinsol:
            canholdatmo = True

    # Generate surface pressure -- range is Mars to Venus, 
    # favoring pressures in the 0.1 to 10 bar range
    # Truncated norm over the range 0.1 - 10, mean of 1.0, stdev of 2.5
    # True upper bound is ~15 bars using this method, very rare
    mean = 1.0
    stdv = 2.5
    lo   = 0.1
    hi   = 90.
    a, b = (lo - mean) / stdv, (hi - mean) / stdv
    rv   = ss.truncnorm(a, b, loc=mean, scale=stdv)
    surfpres = rv.rvs()

    # Generate pressure array
    press = np.logspace(np.log10(pmin), np.log10(surfpres), num=50)

    # Generate atmosphere T(p) profile
    # Sample log10(kappa), log10(gamma1), log10(gamma2), alpha, beta
    kappa  = np.random.uniform(-3.5, -2.0)
    gamma1 = np.random.uniform(-1.5,  1.1)
    gamma2 = np.random.uniform(-1.5,  0.)
    alpha  = np.random.uniform( 0.,   1.)
    # Draw beta from a truncated normal distribution bounded by [0.7, 1.1] w/ mean of 0.95
    mean   = 0.95
    stdv   = 0.1
    lo     = 0.7
    hi     = 1.1
    a, b   = (lo - mean) / stdv, (hi - mean) / stdv
    rv     = ss.truncnorm(a, b, loc=mean, scale=stdv)
    beta   = rv.rvs()
    PTprof = genPT(kappa, gamma1, gamma2, alpha, beta,
                   pmin, surfpres, Rstar, Tstar, sma, surfgrav, nlayers=50)

    # Extract surface temperature
    surftemp = PTprof[0,1]

    # Molecules we are considering
    #         0      1      2     3     4      5      6     7     8      9
    mols = ['H2O', 'CO2', 'O2', 'N2', 'CH4', 'N2O', 'CO', 'O3', 'SO2', 'NH3', 
            'C2H6', 'NO2']
    molweights = np.array([18.02, 44.01, 31.9988, 28.0134, 16.043, 44.013, 
                           28.011, 47.998, 64.06, 17.02, 28.05, 46.0055]) #g/mol

    # Generate molecule abundances - generous ranges
    abuns      = np.zeros(len(mols), dtype=np.float64)
    abuns[0]   = np.random.uniform(0., 0.1)  # H2O gas will be < 10%
    abuns[1:4] = np.random.uniform(0., 1., 3)# CO2, O2, N2 can be any amount
    abuns[4]   = np.random.uniform(0., 0.1)  # 10% CH4 is a LOT
    abuns[5]   = np.random.uniform(0., 0.02) #  2% N2O is a LOT
    abuns[6]   = np.random.uniform(0., 0.02) #  2% CO  is a LOT -- and probably a dead planet
    abuns[7]   = np.random.uniform(0., 0.01 * abuns[2]) # O3 is at most 1% of O2, usually less
    abuns[8]   = np.random.uniform(0., 0.02) # 2% SO2 is a LOT
    abuns[9]   = np.random.uniform(0., 0.01) # 1% NH3 is a LOT, reacts quickly, etc
    abuns[10]  = np.random.uniform(0., 0.01 * abuns[4]) # product of CH4, so at most 1% of that
    abuns[11]  = np.random.uniform(0., 20e-6) # polluting civilization 1000x worse than humans
    
    # Normalize
    abuns /= np.sum(abuns)

    # Calculate mean molecular weight in g/mol
    avgweight = np.sum(molweights * abuns)

    # Generate albedo
    albedo = np.random.uniform(0.1, 0.8)

    # Condense out material
    icloud = [0, 8] #indices of cloud-forming species, H2O and NH3
    clouds = makeclouds(abuns[icloud], PTprof)

    # Repeat abuns for every layer, for easy broadcasting of cloud abundances
    layabuns = np.broadcast_to(abuns, (PTprof.shape[0], len(abuns))).copy()

    # Clouds & hazes
    if np.any(clouds[:,0]!=0) and np.any(clouds[:,1]!=0): #Both water and ammonia clouds
        layabuns[:,icloud] -= clouds #Remove H2O and NH3 gas
        naero = 2
        aeros = 'Water,Ammonia'
        atype = 'AFCRL_WATER_HRI[0.20um-0.03m],Ice_Martonchik_GSFC[0.19-200.00um]'
        aabun = '1,1'
        aunit = 'scl,scl'
        asize = '10,10' #um
        nmax = 8        #Scattering params for clouds
        lmax = 8
    elif np.any(clouds[:,0]!=0): #Only H2O clouds
        clouds = clouds[:,0]
        layabuns[:,0] -= clouds #Remove H2O gas
        naero = 1
        aeros = 'Water'
        atype = 'AFCRL_WATER_HRI[0.20um-0.03m]'
        aabun = '1'
        aunit = 'scl'
        asize = '10'    #um
        nmax = 8        #Scattering params for clouds
        lmax = 8
    elif np.any(clouds[:,1]!=0): #Only NH3 clouds
        clouds = clouds[:,1]
        layabuns[:,1] -= clouds #Remove NH3 gas
        naero = 1
        aeros = 'Ammonia'
        atype = 'Ice_Martonchik_GSFC[0.19-200.00um]'
        aabun = '1'
        aunit = 'scl'
        asize = '10'    #um
        nmax = 8        #Scattering params for clouds
        lmax = 8
    else: #No clouds
        clouds = None
        naero  = 0
        aeros  = ''
        atype  = ''
        aabun  = ''
        aunit  = ''
        asize  = ''
        nmax   = 0
        lmax   = 0
    # Particle size sources:
    # H2O: https://www.atmos-meas-tech.net/10/2105/2017/amt-10-2105-2017.pdf
    #      https://www.goes-r.gov/products/ATBDs/baseline/Cloud_DCOMP_v2.0_no_color.pdf
    #      http://www.curry.eas.gatech.edu/currydoc/Liu_JGR108.pdf
    # NH3: https://www.sciencedirect.com/science/article/pii/0019103582901713
    #      http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.651.9301&rep=rep1&type=pdf

    return starclass, Tstar, Rstar, dist, sma, rplanet*const.R_earth.value/1000., density,  \
           kappa, gamma1, gamma2, alpha, beta,                                              \
           surfpres, PTprof, surftemp, mols, layabuns, abuns, avgweight, albedo,            \
           clouds, naero, aeros, atype, aabun, aunit, asize, nmax, lmax




def callpsg(psg, pname, obstype, cloudsim, timeout_seconds=50, key='key'):
    """
    This function calls PSG with randomly drawn parameters. Observations 
    will be simulated according to the type of host star.

    Inputs
    ------
    psg             - PSG instance from the pypsg package.
    pname           - String. Name of simulted planet.
    obstype         - string. 'do' for direct  observations (F, G, K types), 
                              'tr' for transit observations (M types).
    cloudsim        - bool. Determines whether to simulate scattering of clouds or not.
    timeout_seconds - Int. Delay between subsequent calls. [seconds]
    key             - String. Optional key for special access allowance.

    Outputs
    -------
    params                  - The randomly generated parameters that led to `result'.
    result['spectrum'][:,1] - The measured observation of the planet.
    result['spectrum'][:,2] - The calculated noise associated with the observation.
    result                  - The return from PSG. See result['header'] for more details.
    config                  - The configuration file sent to PSG.
    """

    # Read the default config
    config = psg.default_config

    # Generate parameters
    starclass, Tstar, Rstar, dist, sma, rplanet, density,                               \
           kappa, gamma1, gamma2, alpha, beta,                                          \
           surfpres, PTprof, surftemp, mols, abuns, origabuns, avgweight, albedo,       \
           clouds, naero, aeros, atype, aabun, aunit, asize, nmax, lmax = genparams(obstype)

    # Dictionary of associated HITRAN line lists
    HITDICT = {'H2O':'HIT[1]', 'CO2':'HIT[2]', 'O3':'HIT[3]', 'N2O':'HIT[4]',
               'CO':'HIT[5]', 'CH4':'HIT[6]', 'O2':'HIT[7]', 'NO':'HIT[8]',
               'SO2':'HIT[9]', 'NO2':'HIT[10]', 'NH3':'HIT[11]', 'N2':'HIT[22]',
               'HCN':'HIT[23]', 'C2H6':'HIT[27]', 'H2S':'HIT[31]'}

    # Replace values in config
    # System information
    config['OBJECT-NAME']     = pname               # Planet name
    config['OBJECT-DIAMETER'] = 2. * rplanet        # Planet diameter [km]
    config['OBJECT-GRAVITY']  = density             # Planet density [g/cm3]
    config['OBJECT-STAR-DISTANCE']    = sma         # Semimajor axis [AU]
    config['OBJECT-STAR-TYPE']        = starclass   # Stellar class
    config['OBJECT-STAR-TEMPERATURE'] = Tstar       # Stellar temperature [K]
    config['OBJECT-STAR-RADIUS']      = Rstar       # Stellar radius [Rsun]
    config['GEOMETRY-OBS-ALTITUDE']   = dist        # Distance to system
    if starclass=='M':                              # System geometry
        config['OBJECT-SEASON'] = 180.0             # Transit
    else:
        config['OBJECT-SEASON']          = 90.0     # Greatest separation for direct obs
        config['GEOMETRY-PHASE']         = 90.0
        config['GEOMETRY-SOLAR-ANGLE']   = 77.750
        config['GEOMETRY-STAR-FRACTION'] = 0
    # Atmosphere params
    config['ATMOSPHERE-PRESSURE'] = surfpres        # Planetary surface pressure [bars]
    config['ATMOSPHERE-WEIGHT']   = avgweight       # Molecular weight of atmosphere [g/mol]
    config['ATMOSPHERE-NGAS']     = len(mols)       # Number of gases in atmosphere
    config['ATMOSPHERE-GAS']  = ','.join(mols)      # Gases in atmosphere
    config['ATMOSPHERE-TYPE'] = ','.join([HITDICT[i] for i in mols])    # HITRAN line lists
    config['ATMOSPHERE-ABUN'] = '1,'*(len(abuns[0])-1)+ '1'        # Gas abundances
    config['ATMOSPHERE-UNIT'] = 'scl,'*(len(abuns[0])-1)+ 'scl'    # Unit for abundances
    config['ATMOSPHERE-LAYERS-MOLECULES'] = ','.join(mols)         # Molecules
    for i in range(len(PTprof)):                    # Atmosphere model layer info
        config['ATMOSPHERE-LAYER-'+str(i+1)] = ','.join(map(str, PTprof[i])) + \
                                         ',' + ','.join(map(str, list(abuns[i].astype(str))))
        if cloudsim==True:
            if np.all(clouds != None):                                  # Add cloud info
                if i==0:
                    config['ATMOSPHERE-LAYERS-MOLECULES'] += ',' + aeros    # Cloud names
                if len(clouds.shape) > 1:                               # Cloud abundances
                    config['ATMOSPHERE-LAYER-'+str(i+1)] += ',' + \
                                               ','.join(map(str, list(clouds[i].astype(str))))
                else:
                    config['ATMOSPHERE-LAYER-'+str(i+1)] += ',' + str(clouds[i])
    # Surface info
    config['SURFACE-TEMPERATURE'] = surftemp
    config['SURFACE-ALBEDO']      = albedo
    config['SURFACE-EMISSIVITY']  = 1. - albedo
    # Cloud/aerosol info
    if cloudsim==True:
        config['ATMOSPHERE-NAERO'] = naero
        config['ATMOSPHERE-AEROS'] = aeros
        config['ATMOSPHERE-ATYPE'] = atype
        config['ATMOSPHERE-AABUN'] = aabun
        config['ATMOSPHERE-AUNIT'] = aunit
        config['ATMOSPHERE-ASIZE'] = asize
        config['ATMOSPHERE-NMAX']  = nmax
        config['ATMOSPHERE-LMAX']  = lmax

    # Set observation params based on system type, and necessary resolution for clouds
    if cloudsim==False:
        noisesims = 6
        if starclass=='M':  #Transits
            observations = ['LUVOIR_ECSPEC_02_03',   'LUVOIR_ECSPEC_03_04',  \
                            'LUVOIR_ECSPEC_04_06',   'LUVOIR_ECSPEC_06_08',  \
                            'LUVOIR_ECSPEC_08_13',   'LUVOIR_ECSPEC_13_2',   \
                            'LUVOIR_ECSPEC_2_4',                             \
                            'LUVOIR_ECSPEC_4_8',     'LUVOIR_ECSPEC_8_12',   \
                            'LUVOIR_ECSPEC_12_20',   'LUVOIR_ECSPEC_20_40',  \
                            'LUVOIR_ECSPEC_40_80',   'LUVOIR_ECSPEC_80_160', \
                            'LUVOIR_ECSPEC_160_320', 'LUVOIR_ECSPEC_320_640']
        else:               #Direct observations
            observations = ['LUVOIR_ECLIPS_02_03',   'LUVOIR_ECLIPS_03_04',  \
                            'LUVOIR_ECLIPS_04_06',   'LUVOIR_ECLIPS_06_08',  \
                            'LUVOIR_ECLIPS_08_13',   'LUVOIR_ECLIPS_13_2',   \
                            'LUVOIR_ECLIPS_2_4',                             \
                            'LUVOIR_ECLIPS_4_8',     'LUVOIR_ECLIPS_8_12',   \
                            'LUVOIR_ECLIPS_12_20',   'LUVOIR_ECLIPS_20_40',  \
                            'LUVOIR_ECLIPS_40_80',   'LUVOIR_ECLIPS_80_160', \
                            'LUVOIR_ECLIPS_160_320', 'LUVOIR_ECLIPS_320_640']
    elif cloudsim==True:
        # These need to be updated for the cloud case (smaller intervals)
        noisesims = 6
        if starclass=='M':  #Transits
            observations = ['LUVOIR_ECSPEC_02_03',   'LUVOIR_ECSPEC_03_04',  \
                            'LUVOIR_ECSPEC_04_06',   'LUVOIR_ECSPEC_06_08',  \
                            'LUVOIR_ECSPEC_08_13',   'LUVOIR_ECSPEC_13_2',   \
                            'LUVOIR_ECSPEC_2_4',                             \
                            'LUVOIR_ECSPEC_4_8',     'LUVOIR_ECSPEC_8_12',   \
                            'LUVOIR_ECSPEC_12_20',   'LUVOIR_ECSPEC_20_40',  \
                            'LUVOIR_ECSPEC_40_80',   'LUVOIR_ECSPEC_80_160', \
                            'LUVOIR_ECSPEC_160_320', 'LUVOIR_ECSPEC_320_640']
        else:               #Direct observations
            observations = ['LUVOIR_ECLIPS_02_03',   'LUVOIR_ECLIPS_03_04',  \
                            'LUVOIR_ECLIPS_04_06',   'LUVOIR_ECLIPS_06_08',  \
                            'LUVOIR_ECLIPS_08_13',   'LUVOIR_ECLIPS_13_2',   \
                            'LUVOIR_ECLIPS_2_4',                             \
                            'LUVOIR_ECLIPS_4_8',     'LUVOIR_ECLIPS_8_12',   \
                            'LUVOIR_ECLIPS_12_20',   'LUVOIR_ECLIPS_20_40',  \
                            'LUVOIR_ECLIPS_40_80',   'LUVOIR_ECLIPS_80_160', \
                            'LUVOIR_ECLIPS_160_320', 'LUVOIR_ECLIPS_320_640']
    else:
        print("Invalid specification for cloudsim. Only use True or False.")
        sys.exit()

    # Perform each observation, and store the results and config
    allresults = []
    allconfigs = []

    for i in range(len(observations)):
        rangelo, rangehi, resolution, trans, transshow, transapply,              \
            radunits, telescope, beam, beamunit, diameter,                       \
            telescope1, telescope2, telescope3,                                  \
            noise, noise1, noise2, noiseotemp, noiseoeff, noiseoemis,            \
            noisetime, noiseframe, noisepixel = obs.setobsparams(observations[i])

        # For transit observations, scale exposure/frames based on distance
        if starclass=='M':
            noisetime  = (dist/12.1)**2  * noisetime
        else:
            noisetime  = (dist/ 5.0)**2  * noisetime

        # Fill in config
        config['GENERATOR-RANGE1']      = rangelo
        config['GENERATOR-RANGE2']      = rangehi
        config['GENERATOR-RESOLUTION']  = resolution
        config['GENERATOR-TRANS']       = trans
        config['GENERATOR-TRANS-SHOW']  = transshow
        config['GENERATOR-TRANS-APPLY'] = transapply
        config['GENERATOR-RADUNITS']    = radunits
        config['GENERATOR-TELESCOPE']   = telescope
        config['GENERATOR-BEAM']        = beam
        config['GENERATOR-BEAM-UNIT']   = beamunit
        config['GENERATOR-DIAMTELE']    = diameter
        config['GENERATOR-TELESCOPE1']  = telescope1
        config['GENERATOR-TELESCOPE2']  = telescope2
        config['GENERATOR-TELESCOPE3']  = telescope3
        config['GENERATOR-NOISE']       = noise
        config['GENERATOR-NOISE1']      = noise1
        config['GENERATOR-NOISE2']      = noise2
        config['GENERATOR-NOISEOTEMP']  = noiseotemp
        config['GENERATOR-NOISEOEFF']   = noiseoeff
        config['GENERATOR-NOISEOEMIS']  = noiseoemis
        config['GENERATOR-NOISETIME']   = noisetime
        config['GENERATOR-NOISEFRAMES'] = noiseframe
        config['GENERATOR-NOISEPIXELS'] = noisepixel

        # Store config
        allconfigs.append(config.copy())

        # Run PSG with this config
        success = False # Flag for calculation failures
        try:
            result = psg.run(config)
            success = True
            # Store results
            allresults.append(result.copy())
        except:
            print("Something weird happened?\n")
            break

    # Package parameters for ML
    params = np.zeros(14 + len(abuns[0]) + 2, dtype=np.float64)
    # Order: starclass, Tstar, Rstar, sma, rplanet, density, surfpres,
    #        kappa, gamma1, gamma2, alpha, beta, surftemp,thousands
    #        abuns (vector of length mols), avgweight, albedo
    if   starclass == 'U':
        params[0] = 3
    elif starclass == 'G':
        params[0] = 4
    elif starclass == 'K':
        params[0] = 5
    elif starclass == 'M':
        params[0] = 6
    else:
        print("Error packaging starclass into params.\n")
    params[ 1] = Tstar
    params[ 2] = Rstar
    params[ 3] = dist
    params[ 4] = sma
    params[ 5] = rplanet
    params[ 6] = density
    params[ 7] = surfpres
    params[ 8] = kappa
    params[ 9] = gamma1
    params[10] = gamma2
    params[11] = alpha
    params[12] = beta
    params[13] = surftemp
    for i in range(len(mols)):
        params[14+i] = origabuns[i]
    params[-2] = avgweight
    params[-1] = albedo

    # Package the results, concatenate the configs
    nvals    = [allresults[i]['spectrum'].shape[0] for i in range(len(allresults))]
    results  = np.zeros((np.sum(nvals), 5), dtype=np.float64)
    if starclass=='M':
        inonoise = [0, 1, 3, 4]
    else:
        inonoise = [0, 1,    4]
    #inonoise = [0, 1, 3, 4]

    for i in range(len(allresults)):
        if i < noisesims:
            results[int(np.sum(nvals[:i])) : int(np.sum(nvals[:i]))+nvals[i]] = \
                         allresults[i]['spectrum'][:,:results.shape[1]]
        else:
            try:
                inonoise = [0, 1, 3, 4]
                results[int(np.sum(nvals[:i])) : int(np.sum(nvals[:i]))+nvals[i], inonoise] = \
                         allresults[i]['spectrum'][:,:results.shape[1]-1]
            except:
                inonoise = [0, 1, 4]
                results[int(np.sum(nvals[:i])) : int(np.sum(nvals[:i]))+nvals[i], inonoise] = \
                         allresults[i]['spectrum'][:,:results.shape[1]-2]

    configstrs = [psg.config_dict_to_str(allconfigs[i]) for i in range(len(allconfigs))]
    configs    = '\n\n'.join(configstrs)

    return params, results, configs

