def setobsparams(instr):
    """
    This function defines the observational parameters for various observing
    modes of various telescopes.

    Work in progress. LUVOIR_ECLIPS and LUVOIR_ECSPEC are based on design parameters 
    for LUVOIR, but at a much higher resolution. Note that it is split into multiple 
    channels to allow for scattering calculations.

    Some of the output descriptions are adapted/taken from 
        https://psg.gsfc.nasa.gov/helpapi.php
    See that page for more details. Note there is not a direct match in parameter names, 
    but they are close.


    Inputs
    ------
    instr   - String. Specifies instrument to observe with.

    Outputs
    -------
    rangelo     - Lower range of wavelength [um]
    rangehi     - Upper arnge of wavelength [um]
    resolution  - Resolving power of instrument
    trans       - PSG specification of altitude and atmosphere water %
    transshow   - Y/N flag to determine whether to simulate telluric 
                  absorption of Earth's atmosphere
    transapply  - Y/N flag to determine whether to return the observation as 
                  observed w/ telluric absorption, or to divide by telluric transmittance
    radunits    - Radiation unit of produced spectrum according to PSG's specifications.
    telescope   - Type of telescope. 'SINGLE', 'CORONA', 'ARRAY'
    beam        - FWHM of instrument's beam.
    beamunit    - Unit of `beam`. 'arcsec', 'arcmin', 'degree', 'km', 
                  'diameter' (size in terms of planet diameter), 
                  'diffrac' (defined by telescope diameter and center wavelength)
    diameter    - Diameter of telescope. [m]
    telescope1  - For interferometers, # of telescopes. For coronagraphs, instrument contrast.
    telescope2  - For coronagraphic observations, exozodi level
    telescope3  - For coronagraphic observations, innerworking angle in units [L/D]
    noise       - Noise model specifier. 'NO' (none), 'TRX' (receiver temp/radio), 
                  'RMS' (constant noise in rad units), 'BKG' (constant noise plus background), 
                  'NEP' (power equivalent to noise detector model), 
                  'D' (detectability noise detector model), 'CCD' (image sensor)
    noise1      - First noise model parameter. 
                  For RMS, 1-sigma noise.
                  For TRX, the receiver temperature
                  For BKG, the 1-sigma noise
                  For NEP, the sensitivity in W/sqrt(Hz)
                  For DET, the sensitivity in cm.sqrt(Hz)/W
                  For CCD, the read noise [e-]
    noise2      - Second noise model parameter
                  For RMS, not used
                  For TRX, the sideband g-factor
                  For BKG, the not used
                  For NEP, not used
                  For DET, the pixel size [um]
                  For CCD, the dark rate [e-/s]
    noiseotemp  - Temperature of telescpe+instrument optics [K]
    noiseoeff   - Total throughput of telescope+instrument
    noiseoemis  - Emissivity of telescope+optics
    noisetime   - Exposure time per frame [sec]
    noiseframe  - Number of exposures
    noisepixel  - Number of pixels that encompass the beam
    """
    # JWST NIRISS SOSS, NIRSpec G395M, MIRI LRS:
    # https://arxiv.org/pdf/1611.08608.pdf
    # JWST observation info https://arxiv.org/pdf/1411.1754.pdf
    # https://arxiv.org/pdf/1803.04985.pdf
    #
    """if instr=='JWST-MIRI-LRS':
        rangelo    =
        rangehi    =
        resolution =
        trans      =
        transshow  =
        transapply =
        radunits   =
        telescope  =
        beam       =
        beamunit   =
        diameter   =
        telescope1 =
        telescope2 =
        telescope3 =
        noise      =
        noisetime  =
        noiseframe =
        noisepixel =
        noise1     =
        noise2     =
        noiseoeff  =
        noiseoemis =
        noiseotemp =
        instrument =
    elif instr=='JWST-MIRI-MRS':
        rangelo    =
        rangehi    =
        resolution =
        trans      =
        transshow  =
        transapply =
        radunits   =
        telescope  =
        beam       =
        beamunit   =
        diameter   =
        telescope1 =
        telescope2 =
        telescope3 =
        noise      =
        noisetime  =
        noiseframe =
        noisepixel =
        noise1     =
        noise2     =
        noiseoeff  =
        noiseoemis =
        noiseotemp =
        instrument =
    elif instr=='JWST-NIRISS-SOSS':
        rangelo    =
        rangehi    =
        resolution =
        trans      =
        transshow  =
        transapply =
        radunits   =
        telescope  =
        beam       =
        beamunit   =
        diameter   =
        telescope1 =
        telescope2 =
        telescope3 =
        noise      =
        noisetime  =
        noiseframe =
        noisepixel =
        noise1     =
        noise2     =
        noiseoeff  =
        noiseoemis =
        noiseotemp =
        instrument =
    elif instr=='JWST-NIRSpec-G235H':
        rangelo    =
        rangehi    =
        resolution =
        trans      =
        transshow  =
        transapply =
        radunits   =
        telescope  =
        beam       =
        beamunit   =
        diameter   =
        telescope1 =
        telescope2 =
        telescope3 =
        noise      =
        noisetime  =
        noiseframe =
        noisepixel =
        noise1     =
        noise2     =
        noiseoeff  =
        noiseoemis =
        noiseotemp =
        instrument =
    elif instr=='JWST-NIRSpec-G395H':
        rangelo    =
        rangehi    =
        resolution =
        trans      =
        transshow  =
        transapply =
        radunits   =
        telescope  =
        beam       =
        beamunit   =
        diameter   =
        telescope1 =
        telescope2 =
        telescope3 =
        noise      =
        noisetime  =
        noiseframe =
        noisepixel =
        noise1     =
        noise2     =
        noiseoeff  =
        noiseoemis =
        noiseotemp =
        instrument =
    elif instr=='JWST-NIRCam-F322W2':
        rangelo    =
        rangehi    =
        resolution =
        trans      =
        transshow  =
        transapply =
        radunits   =
        telescope  =
        beam       =
        beamunit   =
        diameter   =
        telescope1 =
        telescope2 =
        telescope3 =
        noise      =
        noisetime  =
        noiseframe =
        noisepixel =
        noise1     =
        noise2     =
        noiseoeff  =
        noiseoemis =
        noiseotemp =
        instrument =
    elif instr=='JWST-NIRCam-F444W':
        rangelo    =
        rangehi    =
        resolution =
        trans      =
        transshow  =
        transapply =
        radunits   =
        telescope  =
        beam       =
        beamunit   =
        diameter   =
        telescope1 =
        telescope2 =
        telescope3 =
        noise      =
        noisetime  =
        noiseframe =
        noisepixel =
        noise1     =
        noise2     =
        noiseoeff  =
        noiseoemis =
        noiseotemp =
        instrument =
    """
    if instr	==	'LUVOIR_ECLIPS_02_03':
        rangelo	    =	0.2
        rangehi     =   0.3
        resolution	=	1900
        telescope	=	'CORONA'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_03_04':
        rangelo	    =	0.3
        rangehi     =   0.4
        resolution	=	1900
        telescope	=	'CORONA'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_04_06':
        rangelo	    =	0.4
        rangehi     =   0.6
        resolution	=	1900
        telescope	=	'CORONA'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_06_08':
        rangelo	    =	0.6
        rangehi     =   0.8
        resolution	=	1900
        telescope	=	'CORONA'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_08_13':
        rangelo	    =	0.8
        rangehi     =   1.3
        resolution	=	1900
        telescope	=	'CORONA'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_13_2':
        rangelo	    =	1.3
        rangehi     =   2.0
        resolution	=	1900
        telescope	=	'CORONA'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_2_4':
        rangelo	    =	2.0
        rangehi     =   4.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2e-6
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_4_8':
        rangelo	    =	4.0
        rangehi     =   8.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2e-6
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_8_12':
        rangelo	    =	8.0
        rangehi     =   12.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2e-6
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_12_20':
        rangelo	    =	12.0
        rangehi     =   20.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2e-6
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_20_40':
        rangelo	    =	20.0
        rangehi     =   40.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_40_80':
        rangelo	    =	40.0
        rangehi     =   80.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_80_160':
        rangelo	    =	80.0
        rangehi     =   160.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_160_320':
        rangelo	    =	160.0
        rangehi     =   320.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECLIPS_320_640':
        rangelo	    =	320.0
        rangehi     =   640.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-10
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.007@2.00e-1,0.008@2.15e-1,0.010@2.40e-1,0.013@2.70e-1,' + \
                        '0.016@3.05e-1,0.018@3.45e-1,0.020@3.80e-1,0.043@4.30e-1,' + \
                        '0.049@4.85e-1,0.049@5.50e-1,0.044@6.25e-1,0.036@7.15e-1,' + \
                        '0.026@8.05e-1,0.044@9.20e-1,0.069@1.06e+0,0.080@1.22e+0,' + \
                        '0.086@1.41e+0,0.090@1.62e+0,0.092@1.87e+0,0.094@2.16e+0,' + \
                        '0.095@2.40e+0,0.096@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	120
        noiseframe	=	240
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'erg'
    elif instr	==	'LUVOIR_ECSPEC_02_03':
        rangelo	    =	0.2
        rangehi     =   0.3
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_03_04':
        rangelo	    =	0.3
        rangehi     =   0.4
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_04_06':
        rangelo	    =	0.4
        rangehi     =   0.6
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_06_08':
        rangelo	    =	0.6
        rangehi     =   0.8
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_08_13':
        rangelo	    =	0.8
        rangehi     =   1.3
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_13_2':
        rangelo	    =	1.3
        rangehi     =   2.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'CCD'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_2_4':
        rangelo	    =	2.0
        rangehi     =   4.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_4_8':
        rangelo	    =	4.0
        rangehi     =   8.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_8_12':
        rangelo	    =	8.0
        rangehi     =   12.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_12_20':
        rangelo	    =	12.0
        rangehi     =   20.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_20_40':
        rangelo	    =	20.0
        rangehi     =   40.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_40_80':
        rangelo	    =	40.0
        rangehi     =   80.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_80_160':
        rangelo	    =	80.0
        rangehi     =   160.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_160_320':
        rangelo	    =	160.0
        rangehi     =   320.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_ECSPEC_320_640':
        rangelo	    =	320.0
        rangehi     =   640.0
        resolution	=	1900
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1.00E-09
        telescope2	=	3
        telescope3	=	2
        noise	    =	'NO'
        noise1	    =	1
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.050@2.00e-1,0.053@2.15e-1,0.068@2.40e-1,0.085@2.70e-1,' + \
                        '0.103@3.05e-1,0.122@3.45e-1,0.132@3.80e-1,0.290@4.30e-1,' + \
                        '0.324@4.85e-1,0.324@5.50e-1,0.296@6.25e-1,0.239@7.15e-1,' + \
                        '0.171@8.05e-1,0.294@9.20e-1,0.459@1.06e+0,0.534@1.22e+0,' + \
                        '0.573@1.41e+0,0.598@1.62e+0,0.615@1.87e+0,'               + \
                        '0.628@2.16e+0,0.636@2.40e+0,0.640@2.50e+0'
        noiseoemis	=	0.1
        noisetime	=	0.2
        noiseframe	=	1.44e5
        noisepixel	=	1
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_HD':
        rangelo	    =	0.2
        rangehi     =   2.2
        resolution	=	4
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1
        telescope2	=	0
        telescope3	=	1
        noise	    =	'CCD'
        noise1	    =	2
        noise2  	=	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.250@2.00e-1,0.267@2.60e-1,0.283@2.75e-1,0.367@4.40e-1,' + \
                        '0.362@5.50e-1,0.333@6.40e-1,0.326@7.90e-1,0.702@1.26e+0,' + \
                        '0.751@1.60e+0,0.772@2.00e+0,0.780@2.20e+0'
        noiseoemis	=	0.1
        noisetime	=	3600
        noiseframe	=	1
        noisepixel	=	8
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr	==	'LUVOIR_LUMOS':
        rangelo	    =	0.1
        rangehi     =   0.4
        resolution	=	500
        telescope	=	'SINGLE'
        diameter	=	15
        beam	    =	1
        beamunit	=	'diffrac'
        telescope1	=	1
        telescope2	=	0
        telescope3	=	1
        noise	    =	'CCD'
        noise1	    =	2
        noise2	    =	1.00E-03
        noiseotemp	=	270
        noiseoeff	=	'0.1'
        noiseoemis	=	0.1
        noisetime	=	3600
        noiseframe	=	1
        noisepixel	=	8
        transapply	=	'N'
        transshow	=	'N'
        trans	    =	'03-01'
        radunits	=	'rel'
    elif instr=='ELT-HIRES':
        rangelo    = 0.4
        rangehi    = 1.8
        resolution = 100000
        trans      = '02-00'
        transshow  = 'Y'
        transapply = 'N'
        radunits   = 'rel'
        telescope  = 'SINGLE'
        beam       = 1
        beamunit   = 'diffrac'
        diameter   = 39.3
        telescope1 = 1
        telescope2 = 0
        telescope3 = 1
        noise      = 'CCD'
        noise1     = 3
        noise2     = 1.00E-02
        noiseotemp = 283
        noiseoeff  = 0.15
        noiseoemis = 0.1
        noisetime  = 1800
        noiseframe = 1
        noisepixel = 8
    """elif instr=='':
        rangelo    =
        rangehi    =
        resolution =
        trans      =
        transshow  =
        transapply =
        radunits   =
        telescope  =
        beam       =
        beamunit   =
        diameter   =
        telescope1 =
        telescope2 =
        telescope3 =
        noise      =
        noisetime  =
        noiseframe =
        noisepixel =
        noise1     =
        noise2     =
        noiseoeff  =
        noiseoemis =
        noiseotemp =
        instrument ="""

    return rangelo, rangehi, resolution, trans, transshow, transapply,          \
           radunits, telescope, beam, beamunit, diameter,                       \
           telescope1, telescope2, telescope3,                                  \
           noise, noise1, noise2, noiseotemp, noiseoeff, noiseoemis,            \
           noisetime, noiseframe, noisepixel

