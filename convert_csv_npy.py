import traceback
import argparse
import numpy as np
import os
import sys
import glob
import pprint
import time
import concurrent.futures
import uuid


def days_hours_mins_secs_str(total_seconds):
    d, r = divmod(total_seconds, 86400)
    h, r = divmod(r, 3600)
    m, s = divmod(r, 60)
    return '{0} days {1:02}:{2:02}:{3:02}'.format(int(d), int(h), int(m), int(s))


def convert(file_name):
    csv_file_name = file_name
    npy_file_name = csv_file_name.replace('.csv', '.npy')
    data = np.loadtxt(csv_file_name, delimiter='\t')
    np.save(npy_file_name, data)
    return npy_file_name


def main():
    try:
        parser = argparse.ArgumentParser(description='NASA FDL 2018 Astrobiology Team 2, Data converter from csv to npy', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument('--convert_all', help='Recursively convert all .csv files under the folder and subfolders to .npy files', default=None, type=str)
        parser.add_argument('--combine_dir', help='Read all .npy files from this folder, combine (stack rows) into a number of .npy files', default=None, type=str)
        parser.add_argument('--combine_max_rows_per_file', help='Number of maximum rows per combined file.', default=100, type=int)
        opt = parser.parse_args()

        print('NASA FDL 2018 Astrobiology Team 2')
        print('INARA: Intelligent exoplaNet Atmosphere RetrievAl\n')
        print('Data converter from csv to npy\n')

        print('Arguments:')
        pprint.pprint(vars(opt), depth=2, width=50)
        print()

        if opt.convert_all is not None:
            files = glob.glob(os.path.join(opt.convert_all, '**/*.csv'), recursive=True)
            if len(files) > 0:
                print('{} csv files found'.format(len(files)))

                with concurrent.futures.ProcessPoolExecutor() as executor:
                    time_start = time.time()
                    i = 0
                    for csv_file_name, npy_file_name in zip(files, executor.map(convert, files)):
                        i += 1
                        duration = time.time() - time_start
                        files_per_sec = (i + 1.) / duration
                        print('File {}/{}, {:,.2} files / second, ETA: {}'.format(i, len(files), files_per_sec, days_hours_mins_secs_str((len(files) - i) / files_per_sec)))
                        print('Reading {}'.format(csv_file_name))
                        print('Writing {}'.format(npy_file_name))
                        # sys.stdout.flush()
            else:
                print('No csv files found in subdirecties under ' + opt.convert_all)

        elif opt.combine_dir is not None:
            files = glob.glob(os.path.join(opt.combine_dir, '*.npy'))
            files = [file for file in files if ('output' not in file) and ('combined' not in file)]
            if len(files) > 0:
                print('{} npy files found'.format(len(files)))
                time_start = time.time()
                combined_data = []
                combined_data_rows = 0
                for i in range(len(files)):
                    duration = time.time() - time_start
                    files_per_sec = (i + 1.) / duration
                    print('File {}/{}, {:,.2} files / second, ETA: {}'.format(i + 1, len(files), files_per_sec, days_hours_mins_secs_str((len(files) - i) / files_per_sec)))
                    print('Reading {}'.format(files[i]))
                    data = np.load(files[i])
                    combined_data.append(data)
                    combined_data_rows += data.shape[0]
                    if (combined_data_rows >= opt.combine_max_rows_per_file) or (i + 1 == len(files)):
                        combined_data = np.concatenate(combined_data)
                        combined_data_file_name = os.path.join(opt.combine_dir, 'combined_{}.npy'.format(uuid.uuid4()))
                        print('*** Writing combined data to: {}'.format(combined_data_file_name))
                        print('Shape: {}'.format(str(combined_data.shape)))
                        np.save(combined_data_file_name, combined_data)
                        combined_data = []
                        combined_data_rows = 0
            else:
                print('No npy files found in subdirecties under ' + opt.combine_dir)
        else:
            print('Either convert_all or combine_dir is expected.')
            sys.exit(1)

    except KeyboardInterrupt:
        print('Stopped.')
    except Exception:
        traceback.print_exc(file=sys.stdout)


if __name__ == '__main__':
    main()
